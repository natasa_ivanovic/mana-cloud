model podataka
	korisnik
		email
		ime
		prezime
		organizacija
		uloga 
	
	organizacija
		ime
		opis
		logo
		lista korisnika
		lista resursa

	virtuelna masina(VM)
		ime
		kategorija
		lista diskova
		lista aktivnosti

	kategorija
		ime
		broj jezgara
		velicina RAM 
		GPU jezgra

	disk
		ime
		tip
		kapacitet u gb
		referenca na VM

funkcionalnost
	Mario %%
	Natasa ##
	svi
		++%%login
		++##logout
		++##izmena profila - v_machines.html -> edit_user.html
		++%%pregled svih VM - v_machines.html
		%%izmena/detalji VM - v_machines.html -> edit_vm.html
			todo: aktivnosti i diskovi
			todo: validacija na serveru i klijentu
		%%filtriranje VM - v_machines.html
		%%filtriranje diskova - disks.html
		##pretraga VM - v_machines.html
		##pretraga diskova - disks.html
		++%%pregled svih diskova - disks.html
		++%%izmena/detalji diska - disks.html -> edit_disk.html
	super admin
		##listanje organizacija - orgs.html
		##dodavanje organizacije - orgs.html -> add_org.html
		++%%brisanje diska - disks.html
		++%%pregled kategorija - cats.html
		++%%dodavanje kategorije - cats.html -> add_cat.html
		++%%izmena kategorija - cats.html -> edit_cat.html
		++%%brisanje kategorije cats.html
	admin i super admin
		##izmena/pregled org - orgs.html -> edit_org.html
		++##pregled korisnika - users.html
		++#dodavanje korisnika - users.html -> add_user.html
		++##izmena/detalji korisnika - users.html -> edit_user.html
		##brisanje korisnika - users.html
		##dodavanje VM - v_machines.html -> add_vm.html
		++%%brisanje VM - v_machines.html
		++%%dodavanje diska - disks.html -> add_disk.html
		##paljenje/gasenje VM (eventualno proveriti da li i super admin moze) - v_machines.html 
	admin
		## mesecni racun - invoice.html

pages:
	login.html 
	link-v_machines.html 
		add_vm.html
		edit_vm.html
		edit_user.html (edituj sebe)
		link-users.html
			add_user.html
			edit_user.html (edituj clanove)
		link-orgs.html
			add_org.html
			edit_org.html
		link-disks.html
			add_disk.html
			edit_disk.html
		link-cats.html
			add_cat.html
			edit_cat.html
		link-invoice.html

bezbednost
	napraviti da se sa zahtevima proverava da li trenutni korisnik sme da pristupi tom resursu
	napraviti slanje da bude sa onim resursima sto korisnik sme
	kada se server startupuje a korisnik postoji da mu se izbrise kuki

nefunkc zahtevi
	uvek mora biti barem jedan super admin
	racunanje mesecne cene je u pdf-u
	super admin kreira prvo org, pa dodaje korisnike u organizaciju
	admini mogu da dodaju obicne korisnike u svoju org (a druge admine?)
	vratiti error messages ako se pogresno pristupi stvarima
	pocetna stranica za sve korisnike je pregled VM
	super admin vidi sve resurse svih organizacija, a admin samo svoje organizacije
	sve izmene se cuvaju u fajlu odmah nakon izvrsavanja iste

sprint 1: do 30.12.
	<3 <3 <3 <3
	N+ napraviti model podataka (klase)
	N+ napraviti osnovni prikaz nekog splash-page-a
	M+ napraviti neku osnovnu komunikaciju front-end - back-end
	
sprint 2: do 1.1.
	M+ napraviti login stranicu i cuvanje/pracenje sesije za korisnika
	N+ napraviti logout i izmenu profila
	
sprint 3: do 6.1
	- napraviti cuvanje i ucitavanje modela iz fajla
		M+ User
		M+ VM
		N- Organization
		M+ Disk
		N- Category

sprint 4: do 7.1
	M- uraditi prikaz VM
	M- uraditi dodavanje VM
	M- uraditi izmenu VM
	N- uraditi prikaz korisnika
	N- uraditi dodavanje korisnika
	N- uopstiti editovanje korisnika
