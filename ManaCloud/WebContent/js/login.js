function checkLogin() {
	var cookie = document.cookie;
	if (cookie != "") {
		window.location.replace("/ManaCloud/v_machines.html");		
	} 
}

checkLogin();

$(document).on('submit', '#loginForm', function(e) {
	e.preventDefault();
	
	var form = this;
	var url = form.action;
	var formData = {
			username: form.username.value,
			password: form.password.value
	}
	var error = false;
	if (formData.username == "") {
		errorDialog(form.username, "Email is a required field!")
		error = true;
	} else {
		clearError(form.username);
		var regExp = /\S+@\S+\.\S+/;
		if (!regExp.test(formData.username)) {
			var message = 'Email must be in the format address@website.domain';
			errorDialog(form.username, message);
			error = true;
		}
	}	
	if (formData.password == "") {
		errorDialog(form.password, "Password is a required field!");
		error = true;
	} else {
		clearError(form.password);
	}
	
	if (error)
		return;
	
	var user = JSON.stringify(formData);
	
	$.ajax({
		type: "POST",
		url: url,
		data: user,
		contentType: "application/json",
		success: function(data)
		{
			handleLoginSuccess(data);
		},
		error: function(data) {
			handleLoginFail(data);
		}
	})
});

function handleLoginSuccess(userData) {
	document.cookie = "username=" + userData.email + "; path=/"; 
	localStorage.setItem("role", userData.role);
	localStorage.setItem("org", userData.organisationName);
	window.location.assign("v_machines.html");		
}

function handleLoginFail(userData) {
	var fieldName = userData.responseText.split(";")[0];
	var message = userData.responseText.split(";")[1];
	var field = $('[name ="' + fieldName + '"]')[0];
	errorDialog(field, message);
	
}


function errorDialog(element, message) {
	element.className = "error";
	element.parentElement.className = "input-group error";
	element.parentElement.childNodes[3].innerText = message;
}

function clearError(element) {
	element.className = "";
	element.parentElement.className = "input-group";
}


function getFormData($form) {
	var result = {};
	$.each($('form').serializeArray(), function() {
	    result[this.name] = this.value;
	});
	return result;
}