/**
 * 
 */

$(document).ready(function() {
	if (localStorage.role != "SUPER_ADMIN")
		window.location.replace = "v_machines.html";
	$.ajax({
		type : "GET",
		url : "rest/organisations",
		success : function(data) {
			renderList(data);
		}, 
		error : function(data) {
			window.location.replace("/ManaCloud/v_machines.html");
		}
	})
})

$(document).on("click", "#add_org", function() {
	window.location.replace("/ManaCloud/add_org.html");
})

function renderList(data) {
	data.forEach(function(org) {
		var tr = $('<tr></tr>');
		var link = "/ManaCloud/edit_org.html";
		tr.append('<td><img id="img" src="' + org.logo + '"height=250px, width=250></img></td>' +
				  '<td>' + org.name + '</td>' +
				  '<td>' + org.description + '</td>' +
				  '<td><a href="' + link + '#' + org.name + '"><input class="edit" type="button" value="Edit"/></a>');
		$("#orgTable").append(tr);
	});
}


