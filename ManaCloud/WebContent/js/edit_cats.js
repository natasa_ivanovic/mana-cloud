/**
 * 
 */
var catID = "";
checkIfValid();
 
$(document).ready(function() {
	var str = window.location.hash.substring(1).replace(/%20/g, ' ');
	catID = str;
	findCat(catID);
})

function checkIfValid() {
	if (window.location.hash == "")
		window.location.replace("/ManaCloud/categories.html");			
	if (localStorage.role != "SUPER_ADMIN")
		window.location.replace("/ManaCloud/categories.html");
}

function findCat(catID) {
	$.ajax({
		type: 'GET',
		url : "rest/categories/" + catID,
		dataType : "json",
		success : function(data) {
			renderCat(data);
		},
		error: function(data) {
			window.location.replace("/ManaCloud/categories.html");
		}
	})
}

function renderCat(data) {
	$('input[name="name"]').val(data.name);
	$('input[name="cores"]').val(data.cores);
	$('input[name="ram"]').val(data.ram);
	$('input[name="gpu"]').val(data.gpu);
}

$(document).on('submit', '#catForm', function(e) {
	e.preventDefault();
	
	var form = this;
	var url = form.action + '/' + catID;
	var method = "PUT";
	
	var cat = {
			name : form.name.value,
			cores : form.cores.value,
			ram: form.ram.value,
			gpu: form.gpu.value
	};
	var error = false;
	if (cat.name == "") {
		var message = 'Name is a required field!';
		errorDialog(form.name, message);
		error = true;
	} else
		clearError(form.name);
	
	if (cat.cores == "") {
		var message =  "Number of cores is a required field";
		errorDialog(form.cores, message);
		error = true;
	} else {
		clearError(form.cores);		
		if (isNaN(cat.cores) || cat.cores <= 0) {
			var message =  "Number of cores must be a positive number greater than 0!";
			errorDialog(form.cores, message);
			error = true;
		} else
			clearError(form.cores);	
	}
	
	if (cat.ram == "") {
		var message =  "RAM is a required field";
		errorDialog(form.ram, message);
		error = true;
	} else {
		clearError(form.ram);		
		if (isNaN(cat.ram) || cat.ram <= 0) {
			var message =  "RAM must be a positive number greater than 0!";
			errorDialog(form.ram, message);
			error = true;
		} else
			clearError(form.ram);	
	}
	
	if (cat.gpu == "") {
		var message =  "Number of GPU cores is a required field";
		errorDialog(form.gpu, message);
		error = true;
	} else {
		clearError(form.gpu);		
		if (isNaN(cat.gpu) || cat.gpu < 0) {
			var message =  "GPU cores must be a positive number greater than 0!";
			errorDialog(form.gpu, message);
			error = true;
		} else
			clearError(form.gpu);	
	}
	
	if (error)
		return;
	data = JSON.stringify(cat);

	$.ajax({
		type: method,
		url: url,
		data: data,
		contentType: "application/json",
		success: function(data)
		{
			handleEditSuccess(data);
		},
		error: function(data) {
			handleError(data);
		}
	})
});

function handleEditSuccess(data) {
	// neki redirect
	window.location.replace("categories.html");
}

function handleError(data) {
	if (data.status == 403) {
		window.location.replace("/ManaCloud/categories.html");
	} else {
		var elements = data.responseText.split(";");
		var fieldName = elements[0];
		var message = elements[1];
		
		var field = $("[name='" + fieldName + "']")[0];
		errorDialog(field, message);
	}
}

$(document).on('click', '#delete', function(e) {
	$.ajax({
		type: "DELETE",
		url: "rest/categories/" + catID,
		success: function(data)
		{
			handleEditSuccess(data);
		},
		error: function(data) {
			handleError(data);
		}
	})
})

$(document).on("click", ".return", function() {
	window.location.replace("categories.html");
})

