/**
 * 
 */
function checkIfValid() {
	if (localStorage.role == "COMMON_USER")
		window.location.replace("/ManaCloud/disks.html");
}

checkIfValid();
 
 // getuj sve organizacije moguce
function loadDisk() {
	$.ajax({
		type : "GET",
		url : "rest/disks/info",
		contentType : "application/json",
		success: function(data) {
			renderForm(data);
		},
		error: function(data) {
			handleError(data);
		}
	})
	
}
$(document).ready(function() {
	loadDisk();
})

function renderForm(data) {
	var selOrg = $("select[name=org]");
	selOrg[0].addEventListener("change", renderVms);
	vmData = data.orgs;
	Object.entries(data.orgs).forEach(function(pair) {
		org = pair[0];
		var input = '<option value="' + org + '" label="' + org + '"/>';
		selOrg.append(input);		
	})
	renderVms();

}
 
function renderVms() {
	Object.entries(vmData).forEach(function(pair) {
		org = pair[0];
		if (org == $('select[name="org"]')[0].value) {
			var selVm = $("select[name=virtualMachine]");
			$("select[name=virtualMachine] option").remove();
			selVm.append('<option value="" label=""/>');
			pair[1].forEach(function(vm) {
			var input = '<option value="' + vm + '" label="' + vm + '"/>';
			selVm.append(input);
			});
		}
	});
} 

$(document).on('submit', '#diskForm', function(e) {
	e.preventDefault();
	
	var form = this;
	
	var disk = {
			name : form.name.value,
			organisation: form.org.value,
			type: form.type.value,
			capacity : form.capacity.value,
			virtualMachine : form.virtualMachine.value
	};
	
	var error = false;
	if (disk.name == "") {
		var message = 'Name is a required field!';
		errorDialog(form.name, message);
		error = true;
	} else
		clearError(form.name);
	
	if (disk.capacity == "") {
		var message =  "Disk capacity is a required field";
		errorDialog(form.capacity, message);
		error = true;
	} else {
		clearError(form.capacity);		
		if (isNaN(disk.capacity) || disk.capacity <= 0) {
			var message =  "Disk capacity must be a positive number greater than 0!";
			errorDialog(form.capacity, message);
			error = true;
		} else
			clearError(form.capacity);	
	}
	if (error)
		return;
		
	$.ajax({
		type : form.method,
		url : form.action,
		data : JSON.stringify(disk),
		contentType : "application/json",
		success: function(msg) {
			//$("#message")[0].innerHTML = msg.responseText;
			window.location.replace("disks.html");
		},
		error: function(msg) {
			handleError(msg);
		}
		
	})
	
})

function handleError(data) {
	if (data.status == 403) {
		window.location.replace("/ManaCloud/disks.html");
	} else {
		var elements = data.responseText.split(";");
		var fieldName = elements[0];
		var message = elements[1];
		
		var field = $("[name='" + fieldName + "']")[0];
		errorDialog(field, message);
	}
}

$(document).on("click", ".return", function() {
	window.location.replace("disks.html");
})