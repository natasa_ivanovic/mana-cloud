/**
 * 
 */
 
$(document).ready(function() {
	if (localStorage.role == "COMMON_USER") {
		$("#add_vm").remove();
	}
})

var rootURL = "rest/virtualMachines"

$(document).ready(function() {
 	console.log('findUser');
	$.ajax({
		type: 'GET',
		url : rootURL,
		contentType: "application/json",
		success : function(data) {
			renderVms(data);
		},
		error: function(data) {
			handleError(data);
		}
	}) 
})
// ime,  jezgra, ram, gpu, org, izmeni
function renderVms(data) {
	data.forEach(function(item) {
		var tr = $('<tr name="data"></tr>');
	    var td = '<td>' + item.vMachine.name + '</td>' +
	            '<td>' + item.vMachine.organisation + '</td>' +
	            '<td>' + item.vMachine.category + '</td>' +
	            '<td>' + item.category.cores + '</td>' +
	            '<td>' + item.category.ram + '</td>' +
	            '<td>' + item.category.gpu + '</td>' +
	            '<td><input type="button" id="' + item.vMachine.name + '" class="edit" name="editVm" value="Edit"></td>';
	    tr.append(td);
	    $('#vmTable').append(tr);
		
	})
}
// u slucaju da nije ulogovan
function handleError(data) {
	redirect();
}

$(document).on("click", 'input[name="editVm"]', function() {
	var id = this.id;
	window.location.assign("/ManaCloud/edit_vm.html#" + id);
})

$(document).on("click", 'input[name="filter"]', function() {
	var cells = $('tr[name="data"]');
	var executeSearch = false;
	$('tr.settings input[type="text"]').each(function() {
		if ($(this).val() != "")
			executeSearch = true;		
	})
	if (!executeSearch) {
		cells.each(function(index, el) {
			el.className = "";					
		});
		return;
	}
	
	cells.each(function(index, el) {
		var name = el.childNodes[0].innerText;
		var org = el.childNodes[1].innerText;
		var cat = el.childNodes[2].innerText;
		var cores = el.childNodes[3].innerText;
		var ram = el.childNodes[4].innerText;
		var gpu = el.childNodes[5].innerText;
		
		var nameMatches = true;
		var orgMatches = true;
		var catMatches = true;
		var coresMatches = true;
		var ramMatches = true;
		var gpuMatches = true;
				
		// check name, org, cat
		if ($("input#name")[0].value.toLowerCase() != "") {
			if (name.toLowerCase().includes($("input#name")[0].value.toLowerCase()))
				nameMatches = true;
			else
				nameMatches = false
		} 
		if ($("input#org")[0].value.toLowerCase() != "") {
			if (org.toLowerCase().includes($("input#org")[0].value.toLowerCase()))
				orgMatches = true;
			else
				orgMatches = false
		} 
		if ($("input#cat")[0].value.toLowerCase() != "") {
			if (cat.toLowerCase().includes($("input#cat")[0].value.toLowerCase()))
				catMatches = true;
			else
				catMatches = false;
		} 
		// check cores, ram, gpu
		var coresMin = $("input#coresMin")[0].value;
		var coresMax = $("input#coresMax")[0].value;
		// bilo koji ima nesto u sebi 
		if (coresMin != "" || coresMax !="") {
			// oba su brojevi
			if (!isNaN(coresMin) && !isNaN(coresMax)) {
				// upada u opseg ili ne
				if (cores >= +($("input#coresMin")[0].value) && cores <= +($("input#coresMax")[0].value))
					coresMatches = true;
				else 
					coresMatches = false;		
				} 
			else
				// jedan od njih nije broj, tkd ne prihvati
				coresMatches = false;
		}
		
		var ramMin = $("input#ramMin")[0].value;
		var ramMax = $("input#ramMax")[0].value;
		// bilo koji ima nesto u sebi 
		if (ramMin != "" || ramMax !="") {
			// oba su brojevi
			if (!isNaN(ramMin) && !isNaN(ramMax)) {
				// upada u opseg ili ne
				if (ram >= +($("input#ramMin")[0].value) && ram <= +($("input#ramMax")[0].value))
					ramMatches = true;
				else 
					ramMatches = false;		
				} 
			else
				// jedan od njih nije broj, tkd ne prihvati
				ramMatches = false;
		}
		
		var gpuMin = $("input#gpuMin")[0].value;
		var gpuMax = $("input#gpuMax")[0].value;
		// bilo koji ima nesto u sebi 
		if (gpuMin != "" || gpuMax !="") {
			// oba su brojevi
			if (!isNaN(gpuMin) && !isNaN(gpuMax)) {
				// upada u opseg ili ne
				var gpuMax = $("input#gpuMax")[0].value;
				if (gpuMax == "")
					gpuMax = "-1";
				if (gpu >= +($("input#gpuMin")[0].value) && gpu <= +gpuMax)
					gpuMatches = true;
				else 
					gpuMatches = false;		
				} 
			else
				// jedan od njih nije broj, tkd ne prihvati
				gpuMatches = false;
		}
			
		
		var matches = nameMatches && orgMatches && catMatches && coresMatches 
					&& ramMatches && gpuMatches;
		if (matches)
			el.className = "selected";
		else
			el.className = "";
	});
})



$(document).on("click", "#add_vm", function() {
	window.location.replace("/ManaCloud/add_vm.html");
})

