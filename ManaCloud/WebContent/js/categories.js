/**
 * 
 */
 
function checkIfValid() {
	if (localStorage.role != "SUPER_ADMIN")
		window.location.replace("/ManaCloud/v_machines.html");
	}
	
checkIfValid();
 
 
var rootURL = "rest/categories"
$(document).ready(function() {
	console.log('getCats');
	$.ajax({
		type: 'GET',
		url : rootURL,
		contentType: "application/json",
		success : function(data) {
			renderCats(data);
		},
		error: function(data) {
			window.location.replace("/ManaCloud/v_machines.html");
		}
	}) 
})

// 	name, cores, ram, gpu	
function renderCats(data) {
	data.forEach(function(item) {
		var tr = $('<tr></tr>');
	    var td = '<td>' + item.name + '</td>' +
	            '<td>' + item.cores + '</td>' +
	            '<td>' + item.ram + '</td>' +
	            '<td>' + item.gpu + '</td>' +
	            '<td><input type="button" id="' + item.name + '" class="edit" name="editCat" value="Edit"></td>';
	    tr.append(td);
	    $('#catsTable').append(tr);
	})
}


$(document).on("click", 'input[name="editCat"]', function() {
	var id = this.id;
	window.location.assign("/ManaCloud/edit_cats.html#" + id);
})

$(document).on("click", "#add_cat", function() {
	window.location.replace("/ManaCloud/add_cat.html");
})

