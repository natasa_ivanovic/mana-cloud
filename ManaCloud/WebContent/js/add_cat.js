/**
 * 
 */
// TODO: proveri da li je trenutni ulogovan super_admin
function checkIfValid() {
	if (localStorage.role != "SUPER_ADMIN")
		window.location.replace("/ManaCloud/categories.html");
}

checkIfValid();

$(document).on('submit', '#catForm', function(e) {
	e.preventDefault();
	
	var form = this;
	
	var cat = {
			name : form.name.value,
			cores : form.cores.value,
			ram: form.ram.value,
			gpu: form.gpu.value
	};
	
	var error = false;
	if (cat.name == "") {
		var message = 'Name is a required field!';
		errorDialog(form.name, message);
		error = true;
	} else
		clearError(form.name);
	
	if (cat.cores == "") {
		var message =  "Number of cores is a required field";
		errorDialog(form.cores, message);
		error = true;
	} else {
		clearError(form.cores);		
		if (isNaN(cat.cores) || cat.cores <= 0) {
			var message =  "Number of cores must be a positive number greater than 0!";
			errorDialog(form.cores, message);
			error = true;
		} else
			clearError(form.cores);	
	}
	
	if (cat.ram == "") {
		var message =  "RAM is a required field";
		errorDialog(form.ram, message);
		error = true;
	} else {
		clearError(form.ram);		
		if (isNaN(cat.ram) || cat.ram <= 0) {
			var message =  "RAM must be a positive number greater than 0!";
			errorDialog(form.ram, message);
			error = true;
		} else
			clearError(form.ram);	
	}
	
	if (cat.gpu == "") {
		var message =  "Number of GPU cores is a required field";
		errorDialog(form.gpu, message);
		error = true;
	} else {
		clearError(form.gpu);		
		if (isNaN(cat.gpu) || cat.gpu < 0) {
			var message =  "GPU cores must be a positive number greater than 0!";
			errorDialog(form.gpu, message);
			error = true;
		} else
			clearError(form.gpu);	
	}
	
	if (error)
		return;
	$.ajax({
		type : form.method,
		url : form.action,
		data : JSON.stringify(cat),
		contentType : "application/json",
		success: function(msg) {
			//$("#message")[0].innerHTML = msg.responseText;
			window.location.replace("categories.html");
		},
		error: function(data) {
			handleError(data);
		}
		
	})
	
})

function handleError(data) {
	if (data.status == 403) {
		window.location.replace("/ManaCloud/categories.html");
	} else {
		var elements = data.responseText.split(";");
		var fieldName = elements[0];
		var message = elements[1];
		
		var field = $("[name='" + fieldName + "']")[0];
		errorDialog(field, message);
	}
}


$(document).on("click", ".return", function() {
	window.location.replace("categories.html");
})
