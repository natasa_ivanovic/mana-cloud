/**
 * 
 */
function checkIfValid() {
	if (localStorage.role == "COMMON_USER")
		$("button#add_disk").remove();
}

 
 var rootURL = "rest/disks"
 $(document).ready(function() {
	checkIfValid();
 	console.log('getDisks');
	$.ajax({
		type: 'GET',
		url : rootURL,
		contentType: "application/json",
		success : function(data) {
			renderDisks(data);
		},
		error: function(data) {
			handleError(data);
		}
	}) 
})
// ime,?tip?, kapacitet, vm
function renderDisks(data) {
	data.forEach(function(item) {
		var tr = $('<tr name="data"></tr>');
	    var td = '<td>' + item.name + '</td>' +
	            '<td>' + item.type + '</td>' +
	            '<td>' + item.capacity + '</td>' +
	            '<td>' + item.virtualMachine+ '</td>' +
	            '<td>' + item.organisation+ '</td>' +
	            '<td><input type="button" id="' + item.name + '" class="edit" name="editDisk" value="Edit"></td>';
	    tr.append(td);
	    $('#diskTable').append(tr);
		
	})
}
 
 $(document).on("click", 'input[name="filter"]', function() {
	    //selektuj sve podatke
		var cells = $('tr[name="data"]');
		
		//provera da li ima sta da se filtrira
		var executeSearch = false;
		$('tr.settings input[type="text"]').each(function() {
			if ($(this).val() != "")
				executeSearch = true;		
		})
		if (!executeSearch) {
			cells.each(function(index, el) {
				el.className = "";	//kliruj sve prethodne rez				
			});
			return;
		}
		
		cells.each(function(index, el) {
			var name = el.childNodes[0].innerText;
			var type = el.childNodes[1].innerText;
			var cap = el.childNodes[2].innerText;
			var vm = el.childNodes[3].innerText;
			var org = el.childNodes[4].innerText;
			
			//svaki search kombinujemo sa and
			var nameMatches = true;
			var typeMatches = true;
			var capMatches = true;
			var vmMatches = true;
			var orgMatches = true;
			
			//eliminisi nepotrebne
			if ($("input#name")[0].value.toLowerCase() != "") {
				if (name.toLowerCase().includes($("input#name")[0].value.toLowerCase()))
					nameMatches = true;
				else
					nameMatches = false
			}
			
			if ($("input#type")[0].value.toLowerCase() != "") {
				if (type.toLowerCase().includes($("input#type")[0].value.toLowerCase()))
					typeMatches = true;
				else
					typeMatches = false
			}
			
			var capMin = $("input#capMin")[0].value;
			var capMax = $("input#capMax")[0].value;
			
			if (capMin != "" || capMax !="") {
				// oba su brojevi
				if (!isNaN(capMin) && !isNaN(capMax)) {
					// upada u opseg ili ne
					if (cap >= +($("input#capMin")[0].value) && cap <= +($("input#capMax")[0].value))
						capMatches = true;
					else 
						capMatches = false;		
					} 
				else
					// jedan od njih nije broj, tkd ne prihvati
					capMatches = false;
			}
			
			if ($("input#vmName")[0].value.toLowerCase() != "") {
				if (vm.toLowerCase().includes($("input#vmName")[0].value.toLowerCase()))
					vmMatches = true;
				else
					vmMatches = false
			}
			
			if ($("input#org")[0].value.toLowerCase() != "") {
				if (org.toLowerCase().includes($("input#org")[0].value.toLowerCase()))
					orgMatches = true;
				else
					orgMatches = false
			}
			
			var matches = nameMatches && typeMatches && capMatches && vmMatches && orgMatches;
			
			if (matches)
				el.className = "selected";
			else
				el.className = "";
			
		});
 })
 
 
 
// u slucaju da nije ulogovan
function handleError(data) {
	window.location.replace("v_machines.html");

}

$(document).on("click", 'input[name="editDisk"]', function() {
	var id = this.id;
	window.location.assign("/ManaCloud/edit_disk.html#" + id);
})

$(document).on("click", "#add_disk", function() {
	window.location.replace("/ManaCloud/add_disk.html");
})
