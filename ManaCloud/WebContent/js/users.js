/**
 * 
 */


$(document).ready(function() {	
	$.ajax({
		type: "GET",
		url : "rest/users",
		success : function(data) {
			renderList(data);
		},
		error: function(data) {
			//nedozvoljena akcija
			alert(data);
			window.location.replace("/ManaCloud/v_machines.html");
		}
	})	
		
})


function renderList(data) {
	//ova kobaja nece trebati
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	//id = usersTable
	$.each(list, function(index,user ) {
		var tr = $('<tr></tr>');
		var link = "/ManaCloud/edit_user.html#" + user.email;
		tr.append('<td>' + user.email + '</td>' +
				  '<td>' + user.name + '</td>' +
				  '<td>' + user.surname + '</td>' +
				  '<td>' + user.organisationName + '</td>' +
				  '<td><a href="' + link + '"><input type="button" class="edit" value="Edit"/></a>');
		$('#usersTable').append(tr);
	});
}

$(document).on("click", "#add_user", function() {
	window.location.replace("/ManaCloud/add_user.html");
})




