/**
 * 
 */

var orgID = "";
checkIfValid();

$(document).ready(function() {
	var str = window.location.hash.substring(1).replace(/%20/g, ' ');
	if (localStorage.role == "SUPER_ADMIN")
		$("#delete").show()
	orgID = str;
	getOrg(orgID); 
})

function checkIfValid() {
	if (window.location.hash == "") {
		window.location.replace("/ManaCloud/orgs.html");
	}
}

function getOrg(orgID) {
	
	$.ajax({
		type : "GET",
		url : "rest/organisations/" + orgID,
		success : function(data) {
			renderOrg(data);
		},
		error : function(data) {
			window.location.replace("/ManaCloud/orgs.html");
		}
		
	})
}

function renderOrg(data) {
	$('input[name="name"]').val(data.name);
	$('textarea[name="description"]').val(data.description);
	$("#img").attr("src", data.logo);
	document.getElementById("logo").value = data.logo;
	
}

$(document).on("click", "#delete_img", function() {
	$("#img").attr("src", "default.jpg");
	document.getElementById("logo").value = "";
}) 

$(document).on("submit", "#orgform", function(e) {
	e.preventDefault();
	
	var form = document.forms["orgform"];
	var organisation = {
			name : form.name.value,
			description : form.description.value,
			logo : form.logo.value
	};
	
	if (organisation.name == "") {
		var message = 'Name is a required field!';
		errorDialog(form.name, message);
		return;
	} else
		clearError(form.name);
	
	
	$.ajax({
		type : "PUT",
		url : "rest/organisations/" + orgID,
		data : JSON.stringify(organisation),
		contentType : "application/json",
		success : function(msg) {
			renderSuccess(msg);
		},
		error : function(msg) {
			renderError(msg);
		}
	})
	
})

$(document).on("click", "#delete", function() {
	$.ajax({
		type: "DELETE",
		url: "rest/organisations/" + orgID,
		success: function(msg)
		{
			window.location.replace("/ManaCloud/orgs.html");
		},
		error: function(msg) {
			$("#error")[0].innerText = "Error deleting! Organization is currently in use!";
			$("#error").show();
		}
	})
})

//uzas xd
function renderError(err) {
	if (data.status == 403) {
		window.location.replace("/ManaCloud/orgs.html");
	} else {
		var elements = data.responseText.split(";");
		var fieldName = elements[0];
		var message = elements[1];
		
		var field = $("[name='" + fieldName + "']")[0];
		errorDialog(field, message);
	}
}

function renderSuccess(data) {
	if (localStorage.role == "ADMIN") {
		$("#success")[0].innerText = "Organization updated successfully!";
		$("#success").show();
		localStorage.org = data;
	} else {
		window.location.replace("/ManaCloud/orgs.html");
	}
}

function readFile() {
	var selectedImg = document.getElementById("picture").files;
	if (selectedImg.length > 0) {
		var picToLoad = selectedImg[0];
	    var FR= new FileReader();
	    
	    FR.addEventListener("load", function(e) {
	      document.getElementById("img").src = e.target.result;
	      document.getElementById("logo").value = e.target.result;
	    }); 
	    
	    FR.readAsDataURL( this.files[0] );
	  }
	  
	}

$(document).ready( function() {
	document.getElementById("picture").addEventListener("change", readFile);
	
})


$(document).on("click", ".return", function() {
	window.location.replace("orgs.html");
})

