/**
 * 
 */

function getOrganisations() {
	if (localStorage.role == "ADMIN") {
		var opt = '<option value=' + localStorage.org +'>' + localStorage.org + '</select>';
		$("#organisation").append(opt);
	} else if (localStorage.role == "SUPER_ADMIN") {
		$.ajax({
			type : "GET",
			url : "rest/organisations",
			success : function(data) {
				renderOrganisation(data);
			},
			error : function(data) {
				//eror je ako nema pravo na tu akciju
				window.location.replace("/ManaCloud/users.html");
			}	
		})
	} else
	window.location.replace("users.html");
}

function renderOrganisation(data) {
	
	data.forEach(function(org) {
		var opt = '<option value=' + org.name +'>' + org.name + '</select>';
		$("#organisation").append(opt);
	})
}

$(document).on('submit', '#userform', function(e) {
	e.preventDefault();
	
	var form = document.forms['userform'];
	
	var user = {
			email : form.email.value,
			name : form.name.value,
			surname : form.surname.value,
			organisationName : form.organisation.value,
			role : form.role.value,
			password : form.password.value		
	};
	
	var error = false;
	if (user.email == "") {
		var message = 'Email is a required field!';
		errorDialog(form.email, message);
		error = true;
	} else {
		clearError(form.email);
		var regExp = /\S+@\S+\.\S+/;
		if (!regExp.test(user.email)) {
			var message = 'Email must be in a format address@website.domain';
			errorDialog(form.email, message);
			error = true;
		} else
			clearError(form.email);	
	}
		
	if (user.name == "") {
		var message = 'Name is a required field!';
		errorDialog(form.name, message);
		error = true;
	} else
		clearError(form.name);
	
	if (user.surname == "") {
		var message = 'Surname is a required field!';
		errorDialog(form.surname, message);
		error = true;
	} else
		clearError(form.surname);
	
	if (user.password == "") {
		var message = 'Password is a required field!';
		errorDialog(form.password, message);
		error = true;
	} else
		clearError(form.password);
	
	if (checkPasswordMatch() == false)
		return;
	
	$.ajax({
		type : "POST",
		url : "rest/users",
		data : JSON.stringify(user),
		contentType : "application/json",
		success: function(msg) {
			//$("#message")[0].innerHTML = msg.responseText;
			window.location.replace("/ManaCloud/users.html");
		},
		error: function(msg) {
			renderError(msg);
		}
		
	})
	
});

function renderError(data) {
	if (data.status == 403) {
		window.location.replace("/ManaCloud/users.html");
	} else {
		var elements = data.responseText.split(";");
		var fieldName = elements[0];
		var message = elements[1];
		
		var field = $("[name='" + fieldName + "']")[0];
		errorDialog(field, message);
	}
}

function checkPasswordMatch() {
	var password = $("#password").val();
	var confirmPassword = $("#confirm_password").val();
	
	if (password != confirmPassword){
		var message = "Passwords don't match!";
		errorDialog($("#confirm_password")[0], message);
		return false;
	}
		
	else {
		clearError($("#confirm_password")[0]);
		return true;
	}
}

$(document).ready(function() {
	getOrganisations();
	$("#confirm_password").keyup(checkPasswordMatch);	
	$("#password").keyup(checkPasswordMatch);
	
});

//$(document).ready(function() {
//});

$(document).on("click", ".return", function() {
	window.location.replace("users.html");
})
