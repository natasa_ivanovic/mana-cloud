/**
 * 
 */

$(document).ready(function () {
	menuRole(localStorage.role);
})

function userProfile() {
	var cookie = document.cookie;
	var username = cookie.split("=")[1];
	window.location.assign("/ManaCloud/edit_profile.html" + "#" + username);
}

function getDateString(date) {
	month = date.getMonth() + 1;
	if (month < 10)
		month = "0" + month;
	day = date.getDate();
	if (day < 10)
		day = "0" + day;
	return date.getFullYear() + "-" + month + "-" + day;
}

function logOut() {
	$.ajax({
		type: "POST",
		url: "rest/users/logout",
		success: function(data)
		{
			redirect();
		},
		error: function(data) {
			redirect();
		}
	})	
}

function redirect() {
	var d = new Date();
	document.cookie = "username=; expires=" + d.toUTCString() + ";path=/";
	localStorage.removeItem("role");
	localStorage.removeItem("org");
	window.location.replace("/ManaCloud/login.html");	
}

function menuRole(role) {
	var list = $("div#menu ul")[0];
	if (role == "ADMIN") {
		list.removeChild(getMenuItem(4));
		getMenuItem(2).firstChild.innerText = "My organization";
		getMenuItem(2).firstChild.href = "edit_org.html#" + localStorage.org;
		
	} else if (role == "COMMON_USER") {
		// skloni users, orgs, cats, invoice
		// 5, 4, 2, 1
		list.removeChild(getMenuItem(5));
		list.removeChild(getMenuItem(4));
		list.removeChild(getMenuItem(2));
		list.removeChild(getMenuItem(1));
	} else {
		list.removeChild(getMenuItem(5));
	}
}

function getMenuItem(num) {
	var el = $("div#menu ul li")[num];
	return el;
}

function checkLogin() {
	var cookie = document.cookie;
	if (cookie == "") {
		window.location.replace("/ManaCloud/login.html");		
	} 
}

function errorDialog(element, message) {
	element.parentElement.className = "input-group error";
	element.parentElement.childNodes[5].innerText = message;
}

function clearError(element) {
	element.parentElement.className = "input-group";
}

checkLogin()