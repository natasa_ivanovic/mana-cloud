/**
 * 
 */



/*function convertPictureBase64() {
	var selectedImg = document.getElementById("picture").files;
	if (selectedImg.length > 0) {
		var picToLoad = selectedImg[0];
		var fileReader = new FileReader();
		
		fileReader.onload = function(fileLoadedEvent) {
			var imgData = fileLoadedEvent.target.result; //base64
			alert(imgData);
		}
		
		return imgData;
	}
	return "";
}*/

function readFile() {
	var selectedImg = document.getElementById("picture").files;
	if (selectedImg.length > 0) {
		var picToLoad = selectedImg[0];
	    var FR= new FileReader();
	    
	    FR.addEventListener("load", function(e) {
	      document.getElementById("img").src = e.target.result;
	      document.getElementById("logo").value = e.target.result;
	    }); 
	    
	    FR.readAsDataURL( this.files[0] );
	  }
	  
	}

$(document).ready( function() {
	if (localStorage.role != "SUPER_ADMIN")
		window.location.replace("orgs.html");
	document.getElementById("picture").addEventListener("change", readFile);
	
})

$(document).on("submit", "#orgform", function(e) {
	e.preventDefault();
	
	var form = document.forms["orgform"];
	//var pic = convertPictureBase64();
	
	var organisation = {
			name : form.name.value,
			description : form.description.value,
			logo : form.logo.value
	};
	
	if (organisation.name == "") {
		var message = 'Name is a required field!';
		errorDialog(form.name, message);
		return;
	} else
		clearError(form.name);
	
	$.ajax({
		type : "POST",
		url : "rest/organisations",
		data : JSON.stringify(organisation),
		contentType : "application/json",
		success : function(msg) {
			window.location.replace("/ManaCloud/orgs.html");
		},
		error : function(msg) {
			renderError(msg);
		}
	})
});

function renderError(err) {
	if (data.status == 403) {
		window.location.replace("/ManaCloud/users.html");
	} else {
		var elements = data.responseText.split(";");
		var fieldName = elements[0];
		var message = elements[1];
		
		var field = $("[name='" + fieldName + "']")[0];
		errorDialog(field, message);
	}
}

$(document).on("click", "#delete_img", function() {
	$("#img").attr("src", "default.jpg");
	document.getElementById("logo").value = "";
}) 



$(document).on("click", ".return", function() {
	window.location.replace("orgs.html");
})
