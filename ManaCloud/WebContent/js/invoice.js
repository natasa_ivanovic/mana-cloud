/**
 * 
 */
 
 $(document).ready(function() {
 	if (localStorage.role != "ADMIN") 
 		window.location.replace("v_machines.html");
 })
 
 
 
 // validiraj datume
 // salji zahtev
 
$(document).on("submit", "#invoice", function(e) {
 	e.preventDefault();
 	var form = this;
 	
 	var dateFromField = form.fromDate;
	var dateFrom = new Date(dateFromField.valueAsNumber);
 	
 	if (isNaN(dateFromField.valueAsNumber)) {
 		$("#error")[0].innerText = "Starting date must be entered!";
 		$("#error").show();
 		return;
 	}
 	
 	var dateToField = form.toDate;
	var dateTo = new Date(dateToField.valueAsNumber);
	if (isNaN(dateToField.valueAsNumber)) {
 		$("#error")[0].innerText = "Ending date must be entered!";
 		$("#error").show();
 		return;
 	}
	
	if (dateFrom > dateTo) {
		$("#error")[0].innerText = "Starting date must be before the ending date!";
 		$("#error").show();
 		return;
	}
	$("#error").hide();
 	
 	data = {
 		fromDate: dateFrom,
 		toDate: dateTo
 	}
 	
 	info = JSON.stringify(data);
 	
 	$.ajax({
 		url: form.action,
 		data: info,
 		method: form.method,
 		contentType: "application/json",
 		success: function(data) {
 			renderData(data);
 		},
 		error: function(data) {
 			handleError(data);
 		}
 		
 	})
 })
 
 function handleError(data) {
 	if (data.status == 403)
 		window.location.replace("v_machines.html");
	else {
		
	} 		
 }
 
 
 // obradi rezultate u tabelu
 
 function renderData(data) {
 	$("table#res tr.data").remove();
 	var table = $("table#res");
 	var total = 0;
 	data.forEach(function(el) {
 		var row = $('<tr class="data"></tr>');
 		var appendString = "<td>" + el.id + "</td>"
 					+"<td>" + el.type + "</td>"
 					+"<td>" + el.hours.toFixed(2) + "</td>"
 					+"<td>" + el.pricePerHour.toFixed(4) + "</td>"
 					+"<td>" + el.totalPrice.toFixed(4) + "</td>";
		total += el.totalPrice;
		row.append(appendString);
		table.append(row); 		
 	})
	var row = $('<tr class="data"></tr>');
 	row.append('<td colspan="4" style="border: 0px;"> </td><td><b>' + total.toFixed(4) + '</b></td>');
 	table.append(row);
 	table.show();
 }