/**
 * 
 */
function checkIfValid() {
	if (localStorage.role == "COMMON_USER")
		window.location.replace("/ManaCloud/v_machines.html");
}

checkIfValid();
 
 // getuj sve organizacije moguce
function loadVM() {
	$.ajax({
		type : "GET",
		url : "rest/virtualMachines/info",
		contentType : "application/json",
		success: function(data) {
			renderForm(data);
		},
		error: function(data) {
			handleError(data);
		}
	})
	
}
 
loadVM();

function renderForm(data) {
	var selOrg = $("select[name=org]");
	selOrg[0].addEventListener("change", renderDisks);
	diskData = data.orgs;
	Object.entries(data.orgs).forEach(function(pair) {
		org = pair[0];
		var input = '<option value="' + org + '" label="' + org + '"/>';
		selOrg.append(input);		
	})
	renderDisks();

	var selVm = $("select[name=category]");
	selVm[0].addEventListener("change", renderCatData);
	catData = data.categories;
	data.categories.forEach(function(cat) {
		var input = '<option value="' + cat.name + '" label="' + cat.name + '"/>';
		selVm.append(input);
	})
	renderCatData();
}
 
 
function renderCatData() {
	var category = $("select[name=category]")[0].value;
	catData.forEach(function(cat) {
		if (category == cat.name) {
			$('input[name="cores"]')[0].value = cat.cores;
			$('input[name="ram"]')[0].value = cat.ram;
			$('input[name="gpu"]')[0].value = cat.gpu;
		}
	})
}

function renderDisks() {
	var disks = $('div#disks');
	disks.empty();
	Object.entries(diskData).forEach(function(pair) {
		org = pair[0];
		if (org == $('select[name="org"]')[0].value) {
			pair[1].forEach(function(diskName) {
				var disk = '<label class="container">' + diskName +
					 		'<input class="disk" type="checkbox" value="' + diskName + '">' +
						 	'<span class="checkmark"></span></label>'
				disks.append(disk);
			});
		}
	});
}

$(document).on('submit', '#vmForm', function(e) {
	e.preventDefault();
	
	var form = this;
	
	var vm = {
			name : form.name.value,
			organisation: form.org.value,
			category: form.category.value,
			disks: []
	};
	
	var error = false;
	if (vm.name == "") {
		var message = 'Name is a required field!';
		errorDialog(form.name, message);
		error = true;
	} else
		clearError(form.name);
	if (vm.category == "") {
		var message = 'Name is a required field!';
		errorDialog(form.category, message);
		error = true;
	} else
		clearError(form.category);
	if (vm.organisation == "") {
		var message = 'Name is a required field!';
		errorDialog(form.org, message);
		error = true;
	} else
		clearError(form.org);
	
	if (error)
		return;
		
	$("input.disk").each(function(index, disk) {
		if (disk.checked)
			vm.disks.push(disk.value);
	})
	
	$.ajax({
		type : form.method,
		url : form.action,
		data : JSON.stringify(vm),
		contentType : "application/json",
		success: function() {
			window.location.replace("v_machines.html");
		},
		error: function(msg) {
			handleError(msg);
		}
		
	})
	
})



function handleError(data) {
	if (data.status == 403) {
		window.location.replace("/ManaCloud/v_machines.html");
	} else {
		var elements = data.responseText.split(";");
		var fieldName = elements[0];
		var message = elements[1];
		
		var field = $("[name='" + fieldName + "']")[0];
		errorDialog(field, message);
	}
}


$(document).on("click", ".return", function() {
	window.location.replace("v_machines.html");
})
