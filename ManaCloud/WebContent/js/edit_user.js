/**
 * 
 */

var userID = "";
checkIfValid();

$(document).ready(function() {
	var str = window.location.hash.substring(1).replace(/%20/g, ' ');
	userID = str;
	getUser(userID);
})

function checkIfValid() {
	if (window.location.hash == "") {
		window.location.replace("/ManaCloud/users.html");
	}
}

function getUser(userID) {	
		$.ajax({
			type: 'GET',
			url : "rest/users/" + userID,
			contentType: "application/json",
			success : function(e) {
				renderUser(e);
			},
			error: function(e) {
				window.location.replace("/ManaCloud/users.html");
			}
		})	
}

$(document).on('submit', '#userform', function(e) {
	e.preventDefault();
	
	var form = document.forms['userform'];
	var organisation;
	
	//super admin nema organizaciju
	if($('#org').length) {
		organisation = form.organisation.value;
	} else {
		organisation = "";
	}
	
	var edited = {
			email : form.email.value,
			name : form.name.value,
			surname : form.surname.value,
			organisationName : organisation,
			role : form.role.value,
			password : form.password.value,
	};
	var error = false;
	if (edited.email == "") {
		var message = 'Email is a required field!';
		errorDialog(form.email, message);
		error = true;
	} else {
		clearError(form.email);
		var regExp = /\S+@\S+\.\S+/;
		if (!regExp.test(edited.email)) {
			var message = 'Email must be in a format address@website.domain';
			errorDialog(form.email, message);
			error = true;
		} else
			clearError(form.email);	
	}
		
	if (edited.name == "") {
		var message = 'Name is a required field!';
		errorDialog(form.name, message);
		error = true;
	} else
		clearError(form.name);
	
	if (edited.surname == "") {
		var message = 'Surname is a required field!';
		errorDialog(form.surname, message);
		error = true;
	} else
		clearError(form.surname);
	
	if (edited.password == "") {
		var message = 'Password is a required field!';
		errorDialog(form.password, message);
		error = true;
	} else
		clearError(form.password);
	
	//confirm pass
	if (checkPasswordMatch() == false)
		return;
	
	if (error)
		return;
	$.ajax({
		type: "PUT",
		url: "rest/users/" + userID,
		data: JSON.stringify(edited),
		contentType: "application/json",
		success: function(data)
		{
			handleEditSuccess(data);
		},
		error: function(data) {
			handleEditFail(data);
		}
	})
});



function checkPasswordMatch() {
	var password = $("#password").val();
	var confirmPassword = $("#confirm_password").val();
	
	if (password != confirmPassword){
		var message = "Passwords don't match!";
		errorDialog($("#confirm_password")[0], message);
		return false;
	}
		
	else {
		clearError($("#confirm_password")[0]);
		return true;
	}
}

$(document).ready(function() {
	$("#confirm_password").keyup(checkPasswordMatch);
});

$(document).ready(function() {
	$("#password").keyup(checkPasswordMatch);
});

function renderUser(data) {
	var user = data.user;
	var org = user.organisationName;
	//super admin ne pripada ni jednoj organizaciji
	if (org == "") {
		$('input[name="organisation"]').attr("disabled", "disabled");
	}
	else {
		$('input[name="organisation"]').val(org);
	}
	
	$('input[name="email"]').val(user.email);
	$('input[name="name"]').val(user.name);
	$('input[name="surname"]').val(user.surname);
	$('input[name="password"]').val(user.password);
	$('input[name="confirm_password"]').val(user.password);
	$('select[name="role"]').val(user.role);
}

function handleEditSuccess(data) {
	//menja sam sebe
	cookie = document.cookie.split("=")[1];
	if (cookie == userID) {
		var elements = data.split(";");
		var fieldID =  elements[0];
		var message = elements[1];
		var cookie = document.cookie.split("=")[1];
		var newID = elements[2];
		
		document.cookie = "username=" + newID + "; path=/";
		var field = $(fieldID)[0];
		$(fieldID).show();
		field.innerHTML = message;
		return;
	}
	
	//ako ne menja sebe da vrati na tabelu
	else {
		window.location.replace("/ManaCloud/users.html");
	}	  
}

function handleEditFail(data) {
	if (data.status == 403) {
		window.location.replace("/ManaCloud/users.html");
	} else {
		var elements = data.responseText.split(";");
		var fieldName = elements[0];
		var message = elements[1];
		if (fieldName == "#error") {
			$(fieldName)[0].innerText = message;
			$(fieldName).show();
			$("#success").hide();
		} 
		else {
			var field = $("[name='" + fieldName + "']")[0];
			errorDialog(field, message);			
		}
	}
}

$(document).on("click", "#del_user", function() {
	
	$.ajax({
		type : "DELETE",
		url : "rest/users/" + userID,
		success : function(msg) {
			//$("#msg")[0].innerHTML = msg.responseText;
			window.location.replace("/ManaCloud/users.html");
		},
		error : function(msg) {
			window.location.replace("/ManaCloud/users.html");
		}
	})
	
})


$(document).on("click", ".return", function() {
	window.location.replace("users.html");
})