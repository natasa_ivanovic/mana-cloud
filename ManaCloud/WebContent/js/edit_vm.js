/**
 * 
 */
var vmID = "";
checkIfValid();
 
$(document).ready(function() {
	if (localStorage.role == "COMMON_USER") {
		$("#activation").remove();
		$('input[name="submit"]').remove();
		$('input.delete').remove();
	} 
	var str = window.location.hash.substring(1).replace(/%20/g, ' ');
	vmID = str;
	findVM(vmID);
})

function checkIfValid() {
	if (window.location.hash == ""){
		window.location.replace("/ManaCloud/v_machines.html");			
	}
}

function findVM(vmID) {
	$.ajax({
		type: 'GET',
		url : "rest/virtualMachines/" + vmID,
		contentType: "application/json",
		dataType : "json",
		success : function(data) {
			renderVM(data);
		},
		error: function(data) {
			window.location.replace("/ManaCloud/v_machines.html")
		}
	})
}

function renderVM(data) {
	// basic info
	$('input[name="name"]').val(data.vMachine.name);
	// tu za kategorije uradi da se stavi event koji ce da promeni vrednosti kada se promeni select >.<
	var selVm = $("select[name=category]");
	selVm[0].addEventListener("change", renderCatData);
	catData = data.categories;
	data.categories.forEach(function(cat) {
		el = cat.name;
		if (el == data.vMachine.category) 
			selVm.append('<option selected value="' + el + '">' + el + "</option>");	
		else
			selVm.append('<option value="' + el + '">' + el + "</option>");
	})
	renderCatData();
	
	$('input#org').val(data.vMachine.organisation);
	// disks 
	// prvo dodaj checkboxove koji su selektovani
	var disksExist = false;
	data.vMachine.disks.forEach(function(disk) {
		var checkbox ='<label class="container">' + disk +
					  '<input class="disk" type="checkbox" value="' + disk + '" checked="checked">' +
					  '<span class="checkmark"></span></label>';
		$('div#disks').append(checkbox);
		disksExist = true;		
	})
	data.disks.forEach(function(disk) {
		var checkbox = '<label class="container">' + disk +
					  '<input class="disk" type="checkbox" value="' + disk + '">' +
					  '<span class="checkmark"></span></label>'
		$('div#disks').append(checkbox);
		disksExist = true;
	})
	if (!disksExist)
		$('div#disks').append('<h3>Currently no disks are available to be added</h3>');
	// activities
	renderActivities(data.vMachine.activities);
}

function renderCatData() {
	var category = $("select[name=category]")[0].value;
	catData.forEach(function(cat) {
		if (category == cat.name) {
			$('input[name="cores"]')[0].value = cat.cores;
			$('input[name="ram"]')[0].value = cat.ram;
			$('input[name="gpu"]')[0].value = cat.gpu;
		};
	});
}

function renderActivities(data) {
	$("table#activityTable tr.data").remove();
	var counter = 0;
	data.forEach(function(act) {
		var turnedOn = new Date(act.turnedOn);
		var turnedOff = new Date(act.turnedOff);
		var tr = $('<tr class="data"></tr>');
		var onTime = getDateString(turnedOn);
		var appendString = '<td><input type="date" name="' + counter + '" class="activityOnDate" value="' + onTime + '"></td>'
						+  '<td><input type="number" name="' + counter +'" class="activityOnNumber" min="0" max="23"' 
						+  'value="' + turnedOn.getHours() + '"></td>';
		if (turnedOff.getTime() != 0) {
			var offTime = getDateString(turnedOff);
			appendString += '<td><input type="date" name="' + counter + '" class="activityOffDate" value="' + offTime + '"></td>'
						+  '<td><input type="number" name="' + counter + '"class="activityOffNumber" min="0" max="23"' 
						+  'value="' + turnedOff.getHours() + '"></td>';
		} else
			appendString += '<td colspan="2">Activity has no end date yet!</td>';
		if (localStorage.role == "COMMON_USER") {
			appendString += '<td><input type="button" id="' + counter +'" class="deleteActivity" disabled="true" value="Delete"/></td>';	
		} else
			appendString += '<td><input type="button" id="' + counter +'" class="deleteActivity" value="Delete"/></td>';
		tr.append(appendString);
		$("table#activityTable").append(tr);
		counter += 1;
	})
	if (localStorage.role == "ADMIN") {
		var activated = false;
		data.forEach(function(activity) {
			if (activity.turnedOff == null)
				activated = true;
		})
		if (activated == true) {
			$("#activation").attr('value', 'Turn off');
			$("#activation")[0].className = "off";
		}
		else {
			$("#activation").attr('value', 'Turn on');
			$("#activation")[0].className = "on";		
		}		
	}
	lockRole();

}

function lockRole() {
	// lockuj da ne moze da edituje 
	if (localStorage.role == "COMMON_USER") {
		$('input').prop("readonly", true);
		$('select').prop("disabled", "disabled");
		$('input[type="checkbox"]').prop("disabled", "disabled");
		$('input[type="date"]').prop("disabled", "disabled");
		$('input[type="number"]').prop("disabled", "disabled");
	}
	if (localStorage.role == "ADMIN") {
		$('input[type="date"]').prop("disabled", "disabled");
		$('input[type="number"]').prop("disabled", "disabled");		
	}
	if (localStorage.role == "SUPER_ADMIN") {
		$('#activation').remove();	
	}
}

$(document).on('submit', '#vmForm', function(e) {
	e.preventDefault();
	
	var form = this;
	var url = form.action + '/' + vmID;
	var method = "PUT";
	
	var vm = {
			name : form.name.value,
			category : form.category.value,
			organisation: form.org.value,
			disks: [],
			activities: [] 
	};
	
	var error = false;
	if (vm.name == "") {
		var message = 'Name is a required field!';
		errorDialog(form.name, message);
		error = true;
	} else
		clearError(form.name);
	if (vm.category == "") {
		var message = 'Name is a required field!';
		errorDialog(form.category, message);
		error = true;
	} else
		clearError(form.category);
	if (vm.organisation == "") {
		var message = 'Name is a required field!';
		errorDialog(form.org, message);
		error = true;
	} else
		clearError(form.org);
	
	if (error)
		return;
	
	$("input.disk").each(function(index, disk) {
		if (disk.checked)
			vm.disks.push(disk.value);
	})
	var turnedOnDate = $("input.activityOnDate");
	var turnedOnHour = $("input.activityOnNumber");
	var turnedOffDate = $("input.activityOffDate");
	var turnedOffHour = $("input.activityOffNumber");
	var counter = turnedOnDate.length;
	for (var i = 0; i != counter; i++) {
		var currentName = turnedOnDate[i].name;
		var turnedOn = new Date(turnedOnDate[i].valueAsNumber);
		turnedOn.setHours(turnedOnHour[i].value);
		var turnedOff = new Date(0);
		// this part kinda needs testing
		if (turnedOffDate.length == counter - 1){
			turnedOffDate.each(function(index, val) {
				if (val.name==currentName) {
					turnedOff = new Date(val.valueAsNumber);
					turnedOff.setHours(turnedOffHour[index].value);
				}
			})
		} else {
			turnedOff = new Date(turnedOffDate[i].valueAsNumber);
			turnedOff.setHours(turnedOffHour[i].value);
		}
		var newTurnedOn = {
			turnedOn: turnedOn,
			turnedOff: turnedOff			
		}
		vm.activities.push(newTurnedOn);
	}
	
	data = JSON.stringify(vm);

	$.ajax({
		type: method,
		url: url,
		data: data,
		contentType: "application/json",
		success: function(data)
		{
			handleEditSuccess(data);
		},
		error: function(data) {
			handleError(data);
		}
	})

	});
	

function handleEditSuccess(data) {
	window.location.replace("v_machines.html");
}


function handleError(data) {
	if (data.status == 403) {
		window.location.replace("/ManaCloud/v_machines.html");
	} else {
		var elements = data.responseText.split(";");
		var fieldName = elements[0];
		var message = elements[1];
		if (fieldName == "#error") {
			$(fieldName)[0].innerText = message;
			$(fieldName).show();
		} else {
			var field = $("[name='" + fieldName + "']")[0];
			errorDialog(field, message);			
		}
	}
}

$(document).on('click', '#delete', function(e) {
	$.ajax({
		type: "DELETE",
		url: "rest/virtualMachines/" + vmID,
		success: function(data)
		{
			handleEditSuccess(data);
		},
		error: function(data) {
			handleError(data);
		}
	})
})


$(document).on("click", ".return", function() {
	window.location.replace("v_machines.html");
})

$(document).on('click', '#activation', function() {
	$.ajax({
		type : "PUT",
		url : "rest/virtualMachines/activity/" + vmID,
		success : function(virtualM) {
			renderActivities(virtualM.activities);
		},
		error : function(data) {
			handleError(data);
		}
	})
})

$(document).on("click", 'input[type="button"].deleteActivity', function() {
	$(this).parent().parent().remove();
})
