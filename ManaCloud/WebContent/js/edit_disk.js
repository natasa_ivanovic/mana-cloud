/**
 * 
 */
 var diskID = "";
 checkIfValid();
 
$(document).ready(function() {
	if (localStorage.role == "COMMON_USER") {
		$('input[name="submit"]').remove();
		$('input.delete').remove();
	}
	var str = window.location.hash.substring(1).replace(/%20/g, ' ');
	diskID = str;			
	findDisk(diskID);
})

function checkIfValid() {
	if (window.location.hash == "")
		window.location.replace("/ManaCloud/disks.html");
}

function lockRole() {
	// lockuj da ne moze da edituje 
	if (localStorage.role == "COMMON_USER") {
		$('#diskForm input').prop("readonly", true);
		$('#diskForm select').prop("disabled", "disabled");
		$('tr.action').remove();
	}
	if (localStorage.role == "ADMIN") {
		$('input#delete').hide();	
	}
}

function findDisk(diskID) {
	$.ajax({
		type: 'GET',
		url : "rest/disks/" + diskID,
		dataType : "json",
		success : function(data) {
			renderDisk(data);
		},
		error: function(data) {
			window.location.replace("/ManaCloud/disks.html");
		}
	})
}

function renderDisk(data) {
	$('input[name="name"]').val(data.disk.name);
	$('input[name="org"]').val(data.disk.organisation);
	$('input[name="capacity"]').val(data.disk.capacity);
	$('select[name="type"]').val(data.disk.type);
	Object.entries(data.orgs).forEach(function(pair) {
		vms = pair[1];
		vms.forEach(function(vm) {
			var selVm = $("select[name=virtualMachine]");
			if (vm==data.disk.virtualMachine) 
				var input = '<option value="' + vm + '" label="' + vm + '" selected/>';
			else
				var input = '<option value="' + vm + '" label="' + vm + '"/>';
			
			selVm.append(input);
		});
	});
	lockRole();
}

$(document).on('submit', '#diskForm', function(e) {
	e.preventDefault();
	
	var form = this;
	var url = form.action + '/' + diskID;
	var method = "PUT";
	var disk = {
				name : form.name.value,
				organisation: form.org.value,
				type: form.type.value,
				capacity : form.capacity.value,
				virtualMachine : form.virtualMachine.value
		};	
	var error = false;
	if (disk.name == "") {
		var message = 'Name is a required field!';
		errorDialog(form.name, message);
		error = true;
	} else
		clearError(form.name);
	
	if (disk.capacity == "") {
		var message =  "Disk capacity is a required field";
		errorDialog(form.capacity, message);
		error = true;
	} else {
		clearError(form.capacity);		
		if (isNaN(disk.capacity) || disk.capacity <= 0) {
			var message =  "Disk capacity must be a positive number greater than 0!";
			errorDialog(form.capacity, message);
			error = true;
		} else
			clearError(form.capacity);	
	}
	if (error)
		return;
	data = JSON.stringify(disk);
	
	$.ajax({
		type: method,
		url: url,
		data: data,
		contentType: "application/json",
		success: function(data)
		{
			handleEditSuccess(data);
		},
		error: function(data) {
			handleEditFail(data);
		}
	})
});

function handleEditSuccess(data) {
	// neki redirect
	window.location.replace("/ManaCloud/disks.html");
}

function handleEditFail(data) {
	if (data.status == 403) {
		window.location.replace("/ManaCloud/disks.html");
	} else {
		var elements = data.responseText.split(";");
		var fieldName = elements[0];
		var message = elements[1];
		
		var field = $("[name='" + fieldName + "']")[0];
		errorDialog(field, message);	
	}
}


$(document).on('click', '#delete', function(e) {
	$.ajax({
		type: "DELETE",
		url: "rest/disks/" + diskID,
		success: function(data)
		{
			handleEditSuccess(data);
		},
		error: function(data) {
			handleEditFail(data);
		}
	})
})

$(document).on("click", ".return", function() {
	window.location.replace("disks.html");
})
