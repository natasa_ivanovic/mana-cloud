package database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import model.VirtualMachine;

public class VirtualMachineDb {
	private static String filePath = "/../ManaCloudDatabase/v_machines.json";

	
	public static ConcurrentHashMap<String,VirtualMachine> load(String path) {
		try {
			path = path + filePath;
			File f = new File(path);
			if (f.exists()) {
				ObjectMapper mapper = new ObjectMapper();
				Map<String, VirtualMachine> machines = mapper.readValue(f, new TypeReference<Map<String, VirtualMachine>>(){});
				return new ConcurrentHashMap<String, VirtualMachine>(machines);				
			}
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;			
	}
	public static void write(String path, ConcurrentHashMap<String, VirtualMachine> db) {
		try {
			String dirPath = path + "/../ManaCloudDatabase";
			File dir = new File(dirPath);
			if (!dir.exists())
				dir.mkdir();
			path = path + filePath;
			File f = new File(path);
			f.delete();
			f.createNewFile();
			
			ObjectMapper mapper = new ObjectMapper();
			ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
			writer.writeValue(f, db);
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
