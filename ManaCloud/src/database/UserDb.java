package database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import model.User;

public class UserDb {
	private static String filePath = "/../ManaCloudDatabase/users.json";

	
	public static ConcurrentHashMap<String,User> load(String path) {
		try {
			path = path + filePath;
			File f = new File(path);
			if (f.exists()) {
				ObjectMapper mapper = new ObjectMapper();
				Map<String, User> users = mapper.readValue(f, new TypeReference<Map<String, User>>(){});
				return new ConcurrentHashMap<String, User>(users);				
			}
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;			
	}
	public static void write(String path, ConcurrentHashMap<String, User> db) {
		try {
			String dirPath = path + "/../ManaCloudDatabase";
			File dir = new File(dirPath);
			if (!dir.exists())
				dir.mkdir();
			path = path + filePath;
			File f = new File(path);
			f.delete();
			f.createNewFile();
			
			ObjectMapper mapper = new ObjectMapper();
			ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
			writer.writeValue(f, db);
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
