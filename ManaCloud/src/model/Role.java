package model;

public enum Role {
	SUPER_ADMIN,
	ADMIN,
	COMMON_USER
}
