package model;

public class User {
	
	private String email; //unique
	private String name;
	private String surname;
	private String organisationName;
	private Role role;
	private String password;
	
	public User() {}
	
	public User(String email, String name, String surname, String organisationName, Role role, String password) {
		super();
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.organisationName = organisationName;
		this.role = role;
		this.password = password;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Email: " + this.email + "\n");
		sb.append("Name: " + this.name + "\n");
		sb.append("Surname: " + this.surname + "\n");
		sb.append("Organisation:\n " + this.organisationName + "\n");
		sb.append("Role: " + this.role + "\n");
		sb.append("--------------");
		return sb.toString();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getOrganisationName() {
		return organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String toFile() {
		String s = String.join("|", email, name, surname, organisationName, role.toString(), password);
		return s;
	}
}
