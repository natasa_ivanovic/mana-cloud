package model;

import java.util.ArrayList;
import java.util.List;

public class Organisation {

	private String name; //unique, required
	private String description;
	private String logo;
	private List<String> users;
	private List<String> virtualMachines;
	private List<String> disks;
	
	public Organisation() {}

	public Organisation(String name) {
		super();
		this.name = name;
		this.description = "";
		this.logo = ""; //picture.jpg?
		this.users = new ArrayList<String>();
		this.virtualMachines = new ArrayList<String>();
		this.disks = new ArrayList<String>();
	}
	
	public Organisation(String name, String description, String logo)
	{
		this.name = name;
		this.description = description;
		this.logo = logo; //picture.jpg?
		this.users = new ArrayList<String>();
		this.virtualMachines = new ArrayList<String>();
		this.disks = new ArrayList<String>();
	}
	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("*/n");
		sb.append("Organisation name: " + this.name + "\n");
		sb.append("Description: " + this.description + "\n");
		sb.append("Logo: " + this.logo + "\n");
		for (String u : this.users)
			sb.append("User: " + u + "\n");
		for (String vm : this.virtualMachines)
			sb.append("Virtual machine: " + vm + "\n");
		for (String d : this.disks)
			sb.append("Disk: " + d + "\n");
		
		return sb.toString();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public List<String> getUsers() {
		return users;
	}

	public void setUsers(List<String> users) {
		this.users = users;
	}

	public List<String> getVirtualMachines() {
		return virtualMachines;
	}

	public void setVirtualMachines(List<String> virtualMachines) {
		this.virtualMachines = virtualMachines;
	}

	public List<String> getDisks() {
		return disks;
	}

	public void setDisks(List<String> disks) {
		this.disks = disks;
	}


	
	
	
	
}
