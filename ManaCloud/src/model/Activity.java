package model;

import java.util.Date;

public class Activity {

	private Date turnedOn;
	private Date turnedOff;
	
	public Activity()
	{
		this.turnedOn = new Date();
		this.turnedOff = null;
	}
	
	@Override
	public String toString() {
		return "Turned on: " + this.turnedOn + "," + "turned off: " + this.turnedOff; 
	}

	public Date getTurnedOn() {
		return turnedOn;
	}

	public void setTurnedOn(Date turnedOn) {
		this.turnedOn = turnedOn;
	}

	public Date getTurnedOff() {
		return turnedOff;
	}

	public void setTurnedOff(Date turnedOff) {
		this.turnedOff = turnedOff;
	}
	
	
	
}
