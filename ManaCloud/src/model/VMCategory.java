package model;

public class VMCategory {

	private String name; //unique, required
	private int cores; //required > 0
	private double ram; //required > 0
	private int gpu;
	
	public VMCategory() {}

	public VMCategory(String name, int cores, double ram, int gpu) {
		super();
		this.name = name;
		this.cores = cores;
		this.ram = ram;
		this.gpu = gpu;
	}
	

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Name: " + this.name + "\n");
		sb.append("Cores: " + this.cores + "\n");
		sb.append("RAM: " + this.ram + "\n");
		sb.append("GPU: " + this.gpu + "\n");
		
		return sb.toString();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCores() {
		return cores;
	}

	public void setCores(int cores) {
		this.cores = cores;
	}

	public double getRam() {
		return ram;
	}

	public void setRam(double ram) {
		this.ram = ram;
	}

	public int getGpu() {
		return gpu;
	}

	public void setGpu(int gpu) {
		this.gpu = gpu;
	}
	
	
}
