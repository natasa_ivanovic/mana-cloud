package model;

import java.util.ArrayList;
import java.util.List;

public class VirtualMachine {
	
	private String name; //unique, required
	private String category; //required
	private String organisation;
	private List<String> disks;
	private List<Activity> activities;
	
	public VirtualMachine() {}

	public VirtualMachine(String name, String category, String organisation) {
		super();
		this.name = name;
		this.category = category;
		this.organisation = organisation;
		this.disks = new ArrayList<String>();
		this.activities = new ArrayList<Activity>();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Name: " + this.name + "\n");
		sb.append("Category: " + this.category + "\n");
		sb.append("Organisation: " + this.organisation + "\n");
		for (String d : this.disks)
			sb.append("Disc: " + d + "\n");
		for (Activity a : this.activities)
			sb.append("Activity: " + a);
		
		return sb.toString();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<String> getDisks() {
		return disks;
	}

	public void setDisks(List<String> disks) {
		this.disks = disks;
	}

	public List<Activity> getActivities() {
		return activities;
	}

	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}
	// TODO update ovo za citanje fajlova
	// ime, kat, disks, aktivnosti
	public String toFile() {
		String s = String.join("|", name, category, disksToString(), activitiesToString());
		return s;
	}

	private String activitiesToString() {
		if (this.activities.isEmpty())
			return " ";
		StringBuilder sb = new StringBuilder();
		for (Activity a : this.activities) {
			sb.append(a.getTurnedOn().getTime());
			sb.append("-");
			if (a.getTurnedOff() == null)
				sb.append(0);
			else
				sb.append(a.getTurnedOff().getTime());
			sb.append(";");
		}
		if (this.activities.size() > 1) {
			sb.setLength(sb.length() - 1);
		}
		return sb.toString();
	}

	private String disksToString() {
		if (this.disks.isEmpty())
			return "";
		StringBuilder sb = new StringBuilder();
		for (String d : this.disks) {
			sb.append(d);
			sb.append(";");
		}
		if (this.activities.size() > 1) {
			sb.setLength(sb.length() - 1);
		}
		return sb.toString();
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	
}
