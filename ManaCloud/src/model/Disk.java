package model;

public class Disk {

	private String name; //unique, required
	private DiskType type; //required
	private double capacity; //required
	private String virtualMachine;
	private String organisation;
	
	public Disk() {}

	public Disk(String name, DiskType type, double capacity, String virtualMachine, String organisation) {
		super();
		this.name = name;
		this.type = type;
		this.capacity = capacity;
		this.virtualMachine = virtualMachine;
		this.organisation = organisation;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Name: " + this.name + "\n");
		sb.append("Type: " + this.type.name() + "\n");
		sb.append("Capacity: " + this.capacity + "\n");
		sb.append("Virtual machine: " + this.virtualMachine);
		sb.append("Organisation: "+ this.organisation);
		return sb.toString();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DiskType getType() {
		return type;
	}

	public void setType(DiskType type) {
		this.type = type;
	}

	public double getCapacity() {
		return capacity;
	}

	public void setCapacity(double capacity) {
		this.capacity = capacity;
	}

	public String getVirtualMachine() {
		return virtualMachine;
	}

	public void setVirtualMachine(String virtualMachine) {
		this.virtualMachine = virtualMachine;
	}
	// name, type, capacity, VM TODO UPDATE OVO!!!!
	public String toFile() {
		String s = String.join("|", name, type.name(), Double.toString(capacity), virtualMachine);
		return s;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}
	
	
	
}
