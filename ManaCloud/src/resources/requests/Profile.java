package resources.requests;

import model.User;

public class Profile {
	
	private User user;
	private String id;
	
	public Profile() {}

	public Profile(User user, String id) {
		super();
		this.user = user;
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
}
