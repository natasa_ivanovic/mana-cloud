package resources.requests;

public class UserId {
	
	private String username;
	
	public UserId() {}

	public UserId(String username) {
		super();
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	

}
