package resources.requests;

import model.VirtualMachine;

public class VMRequest {
	private VirtualMachine data;
	private String id;
	
	public VMRequest() {}
	
	public VMRequest(VirtualMachine data, String id) {
		super();
		this.data = data;
		this.id = id;
	}
	public VirtualMachine getData() {
		return data;
	}
	public void setData(VirtualMachine data) {
		this.data = data;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
}
