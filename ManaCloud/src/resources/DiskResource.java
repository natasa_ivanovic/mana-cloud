package resources;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import database.DiskDb;
import model.Disk;
import model.DiskType;
import model.Organisation;
import model.Role;
import model.User;
import model.VirtualMachine;
import resources.responses.DiskAddResponse;

@Path("/disks")
public class DiskResource {
	@Context
	ServletContext ctx;

	@Context
	HttpServletRequest request;

	private static ConcurrentHashMap<String, Disk> diskDB = new ConcurrentHashMap<>();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response addDisk(Disk disk) {
		// check da li je uopste iko ulogovan
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		
		if (checkUser == null)
			return Response.status(403).build();
		
		// check da li je super admin
		if (checkUser.getRole().equals(Role.COMMON_USER))
			return Response.status(403).build();
		// check da li je zahtev ok
		String message = validateFields(disk);
		if (!message.equals(""))
			return Response.status(400).entity(message).build();
		
		if (diskDB.containsKey(disk.getName().toLowerCase()))
			return Response.status(400).entity("name;Name has to be unique!").build();
		
		if (!disk.getVirtualMachine().equals("")) {
			String vm = disk.getVirtualMachine();
			//ne moze se desiti preko forme
			if (!VirtualMachineResource.getDB().get(vm.toLowerCase()).getOrganisation().equals(disk.getOrganisation()))
				return Response.status(403).build();
		}
		diskDB.put(disk.getName().toLowerCase(), disk);
		OrganisationResource.getDB().get(disk.getOrganisation().toLowerCase()).getDisks().add(disk.getName());
		OrganisationResource.writeOrgs(ctx.getRealPath("/"));
		if (!disk.getVirtualMachine().equals("")) {
			VirtualMachineResource.getDB().get(disk.getVirtualMachine().toLowerCase()).getDisks().add(disk.getName());
			VirtualMachineResource.writeVms(ctx.getRealPath("/"));
		}
		writeDisks(ctx.getRealPath("/"));
		return Response.ok().build();
	}

	@GET
	@Path("/info")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAddInfo() {
		User user = (User) request.getSession().getAttribute("loggedInUser");
		if (user == null)
			return Response.status(403).build();
		if (user.getRole().equals(Role.COMMON_USER))
			return Response.status(403).build();
		
		DiskAddResponse res = new DiskAddResponse();
		if (user.getRole().equals(Role.SUPER_ADMIN)) {
			for (Organisation org : OrganisationResource.getDB().values()) {
				ArrayList<String> vms = new ArrayList<String>();
				for (String vm : org.getVirtualMachines()) {
					vms.add(vm);
					res.getOrgs().put(org.getName(), vms);
				}
			}
		} else {
			ArrayList<String> vms = new ArrayList<String>();
			for (String vm : OrganisationResource.getDB().get(user.getOrganisationName().toLowerCase())
					.getVirtualMachines())
				vms.add(vm);
			res.getOrgs().put(user.getOrganisationName(), vms);
		}
		return Response.status(200).entity(res).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllDisks() {
		// u slucaju da nije ulogovan i da pristupa stranici
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		if (checkUser.getRole().equals(Role.SUPER_ADMIN))
			return Response.status(200).entity(diskDB.values()).build();
		ArrayList<Disk> disks = new ArrayList<Disk>();
		for (Disk d : diskDB.values()) {
			if (d.getOrganisation().equals(checkUser.getOrganisationName()))
				disks.add(d);
		}
		return Response.status(200).entity(disks).build();
	}

	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDisk(@PathParam("id") String id) {
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		
		if (id == null || id.equals(""))
			return Response.status(403).build();
		
		if (!diskDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		
		Disk d = diskDB.get(id.toLowerCase());
		// check da li je super admin ili admin/korisnik
		if ((checkUser.getRole().equals(Role.COMMON_USER) || checkUser.getRole().equals(Role.ADMIN))
				&& !checkUser.getOrganisationName().equals(d.getOrganisation()))
			return Response.status(403).build();
		
		DiskAddResponse res = new DiskAddResponse(d);
		ArrayList<String> vms = new ArrayList<String>();
		for (String vm : OrganisationResource.getDB().get(d.getOrganisation().toLowerCase()).getVirtualMachines()) {
			vms.add(vm);
		}
		res.getOrgs().put(d.getOrganisation(), vms);
		return Response.status(200).entity(res).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response editDisk(@PathParam("id") String id, Disk newDisk) {
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		
		// check da li je super admin
		if (checkUser.getRole().equals(Role.COMMON_USER))
			return Response.status(403).build();
		
		if (id == null || id.equals(""))
			return Response.status(403).build();
		
		String message = validateFields(newDisk);
		if (!message.equals(""))
			return Response.status(400).entity(message).build();
		
		if (!diskDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		
		Disk disk = diskDB.get(id.toLowerCase());
		if (!id.equals(newDisk.getName())) {
			// menja se ime, brisi iz recnika i opet stavi
			if (diskDB.containsKey(newDisk.getName().toLowerCase()))
				return Response.status(400).entity("name;Name has to be unique!").build();
			disk.setCapacity(newDisk.getCapacity());
			disk.setName(newDisk.getName());
			disk.setType(newDisk.getType());
			if (!disk.getVirtualMachine().equals("")) {
				// postojeci -> novi, postojeci -> null
				if (!disk.getVirtualMachine().equals(newDisk.getVirtualMachine())) {
					// desila se promena vm, ako nije promenio na null, odvezi sa stare stari id i
					// prevezi na novu sa novim id-om
					if (!newDisk.getVirtualMachine().equals("")) {
						VirtualMachineResource.getDB().get(disk.getVirtualMachine().toLowerCase()).getDisks()
								.remove(id);
						VirtualMachineResource.getDB().get(newDisk.getVirtualMachine().toLowerCase()).getDisks()
								.add(newDisk.getName());
					}
					// inace samo skloni sa stare stari id
					else {
						VirtualMachineResource.getDB().get(disk.getVirtualMachine().toLowerCase()).getDisks()
								.remove(id);
					}
				} else {
					// postojeci -> postojeci, jer je ime promenjeno
					// nije menjan vm, samo promeni ime
					VirtualMachineResource.getDB().get(disk.getVirtualMachine().toLowerCase()).getDisks().remove(id);
					VirtualMachineResource.getDB().get(disk.getVirtualMachine().toLowerCase()).getDisks()
							.add(newDisk.getName());
				}
				VirtualMachineResource.writeVms(ctx.getRealPath("/"));
			} else {
				// null -> new, inace nista
				if (!newDisk.getVirtualMachine().equals("")) {
					VirtualMachineResource.getDB().get(newDisk.getVirtualMachine().toLowerCase()).getDisks()
					.add(id);
					VirtualMachineResource.writeVms(ctx.getRealPath("/"));
				}
			}
			disk.setVirtualMachine(newDisk.getVirtualMachine());
			diskDB.remove(id.toLowerCase());
			diskDB.put(disk.getName().toLowerCase(), disk);
			OrganisationResource.getDB().get(disk.getOrganisation().toLowerCase()).getDisks().remove(id);
			OrganisationResource.getDB().get(disk.getOrganisation().toLowerCase()).getDisks().add(newDisk.getName());
			OrganisationResource.writeOrgs(ctx.getRealPath("/"));
		} else {
			// ne menja se ime, samo zameni stvari
			disk.setCapacity(newDisk.getCapacity());
			disk.setType(newDisk.getType());
			if (!disk.getVirtualMachine().equals("")) {
				// postojeci -> novi, postojeci -> null, inace samo nemoj nista
				if (!disk.getVirtualMachine().equals(newDisk.getVirtualMachine())) {
					// menjao se vm, odvezi sa stare i prevezi na novu (id se nije menjao)
					if (!newDisk.getVirtualMachine().equals("")) {
						VirtualMachineResource.getDB().get(disk.getVirtualMachine().toLowerCase()).getDisks()
								.remove(id);
						VirtualMachineResource.getDB().get(newDisk.getVirtualMachine().toLowerCase()).getDisks()
								.add(id);
					} else {
						VirtualMachineResource.getDB().get(disk.getVirtualMachine().toLowerCase()).getDisks()
								.remove(id);
					}
					VirtualMachineResource.writeVms(ctx.getRealPath("/"));
				}
			} else {
				// null -> new, inace nista
				if (!newDisk.getVirtualMachine().equals("")) {
					VirtualMachineResource.getDB().get(newDisk.getVirtualMachine().toLowerCase()).getDisks()
					.add(id);
					VirtualMachineResource.writeVms(ctx.getRealPath("/"));
				}
			}
			disk.setVirtualMachine(newDisk.getVirtualMachine());

		}
		// pisi u fajl
		writeDisks(ctx.getRealPath("/"));
		return Response.status(200).build();
	}

	@Path("/{id}")
	@DELETE
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteDisk(@PathParam("id") String id) {
		User user = (User) request.getSession().getAttribute("loggedInUser");
		if (user == null)
			return Response.status(403).build();
		
		if (id == null || id.equals(""))
			return Response.status(403).build();
		
		if (!user.getRole().equals(Role.SUPER_ADMIN))
			return Response.status(403).build();
		
		if (!diskDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		Disk d = diskDB.get(id.toLowerCase());
		// skloni iz vm-ova
		if (!"".equals(d.getVirtualMachine())) {
			VirtualMachine vm = VirtualMachineResource.getDB().get(d.getVirtualMachine().toLowerCase());
			vm.getDisks().remove(id);
			VirtualMachineResource.writeVms(ctx.getRealPath("/"));			
		}
		// skloni iz mape
		diskDB.remove(id.toLowerCase());
		writeDisks(ctx.getRealPath("/"));
		return Response.ok().build();
	}

	@PostConstruct
	public void init() {
		loadDisks(ctx.getRealPath("/"));
		OrganisationResource.loadOrgs(ctx.getRealPath("/"));
		VirtualMachineResource.loadVms(ctx.getRealPath("/"));
	}

	static void loadDisks(String path) {
		if (diskDB.isEmpty()) {
			ConcurrentHashMap<String, Disk> disks = DiskDb.load(path);
			if (disks != null) {
				diskDB = disks;
				return;
			}
			// ako se desila greska u ucitavanju, ucitaj ove defaultne
			Disk d = new Disk("SG300BQ", DiskType.HDD, 10000, "VM1", "Cloud");
			diskDB.put(d.getName().toLowerCase(), d);
			Disk d2 = new Disk("Samsung EVO820", DiskType.SSD, 256, "VM2", "Rainbow");
			diskDB.put(d2.getName().toLowerCase(), d2);
			writeDisks(path);
		}
	}

	public static void writeDisks(String path) {
		DiskDb.write(path, diskDB);
	}

	public static Map<String, Disk> getDB() {
		return diskDB;
	}
	
	private String validateFields(Disk d) {
		if (d.getName() == null || d.getName().equals(""))
			return "name;This is a required field!";
		if (d.getType() == null || d.getType().toString().equals(""))
			return "type;This is a required field!";
		if (d.getCapacity() <= 0)
			return "capacity;Value greater or equal to 0 required!";
		return "";
	}
}
