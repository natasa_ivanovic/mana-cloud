package resources;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import database.UserDb;
import model.Organisation;
import model.Role;
import model.User;
import resources.requests.Credentials;
import resources.requests.Profile;
import resources.responses.GenericResponse;

@Path("/users")
public class UserResource {

	@Context
	ServletContext ctx;

	@Context
	HttpServletRequest request;

	private static ConcurrentHashMap<String, User> userDB = new ConcurrentHashMap<>();

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserData(@PathParam("id") String id)
	{ 
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		
		if (id == null || id.equals(""))
			return Response.status(403).build();
		
		if (!userDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		
		Profile profile = new Profile();
		User user = userDB.get(id.toLowerCase());
		profile.setId(id);
		profile.setUser(user);
		return Response.status(200).entity(profile).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUsers()
	{
		
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		
		if (checkUser.getRole().equals(Role.COMMON_USER))
			return Response.status(403).build();
		
		List<User> allUsers = new ArrayList<>();
		
		Role role = checkUser.getRole();
		String username = checkUser.getEmail();
		
		if (role.equals(Role.SUPER_ADMIN))
		{
			for (User u : userDB.values())
			{
				if (u.getEmail().equals(username))
					continue;
				else
					allUsers.add(u);
			}
			return Response.status(200).entity(allUsers).build();
		}
		
		if (role.equals(Role.ADMIN))
		{
			String org = checkUser.getOrganisationName();
			for (User u : userDB.values())
			{
				if (u.getEmail().equals(username))
					continue;
				else if (u.getOrganisationName().equals(org)) {
					if (u.getRole().equals(Role.COMMON_USER) || u.getRole().equals(Role.ADMIN))
						allUsers.add(u);
				}
			}
			return Response.status(200).entity(allUsers).build();
		}
		return Response.status(403).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response edit(@PathParam("id") String id, User editedUser)
	{
		User currentUser = (User) request.getSession().getAttribute("loggedInUser");
		
		if (currentUser == null)
			return Response.status(403).build();
		
		if (id == null || id.equals(""))
			return Response.status(403).build();
		
		if (!userDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		
		String message = validateEmptyFields(editedUser);
		if (!message.equals(""))
			return Response.status(400).entity(message).build();

		//menja sam sebe
		if (id.toLowerCase().equals(currentUser.getEmail().toLowerCase()))
		{
			String newUsername = editedUser.getEmail();
			String oldUsername = currentUser.getEmail();
			
			if (userDB.containsKey(newUsername))
			{
				//znaci da je uneto tudje korisnicko ime 
				if (!newUsername.equals(oldUsername)) {
					return Response.status(400).entity("email;Username has to be unique!").build();
				}
			}
			 
			Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
			
			Matcher matcher = pattern.matcher(newUsername);
			if (!matcher.find())
				return Response.status(400).entity("email;Email has to be in format address@website.domain!").build();
			
			currentUser.setEmail(newUsername);
			currentUser.setName(editedUser.getName());
			currentUser.setSurname(editedUser.getSurname());
			currentUser.setOrganisationName(editedUser.getOrganisationName());
			currentUser.setPassword(editedUser.getPassword());
			
			userDB.remove(oldUsername.toLowerCase());
			userDB.put(newUsername.toLowerCase(), currentUser);
			//upisi izmenu u fajl!
			writeUsers(ctx.getRealPath("/"));
			OrganisationResource.writeOrgs(ctx.getRealPath("/"));
			ctx.setAttribute("loggedInUser", currentUser);
			return Response.status(200).entity("#success;Profile successfully updated!;" + newUsername).build();
			
		}
		else
		{
			//nema problema oko username jer ga ne mogu menjati
			userDB.remove(id.toLowerCase());
			userDB.put(id.toLowerCase(), editedUser);
			//upisati u fajl
			writeUsers(ctx.getRealPath("/"));
			return Response.status(200).build(); //redirekcija na sve korisnike
		}
		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response addUser(User user)
	{
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build(); //forbidden
		
		if (checkUser.getRole().equals(Role.COMMON_USER))
			return Response.status(403).build(); //forbidden
		
		String message = validateEmptyFields(user);
		if (!message.equals(""))
			return Response.status(400).entity(message).build();
		
		//email mora biti jedinstven
		if (userDB.containsKey(user.getEmail().toLowerCase()))
			return Response.status(400).entity("email;Username has to be unique!").build();
		

		Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
		
		Matcher matcher = pattern.matcher(user.getEmail());
		if (!matcher.find())
			return Response.status(400).entity("email;Email has to be in format address@website.domain!").build();
		
		
		if (checkUser.getRole().equals(Role.ADMIN) && !checkUser.getOrganisationName().equals(user.getOrganisationName()))
			return Response.status(403).build();
		String orgName = user.getOrganisationName().toLowerCase();
		
		if (!OrganisationResource.getDB().containsKey(orgName))
			return Response.status(403).build();
		
		//dodati korisnika u listu organizacije
		OrganisationResource.getDB().get(orgName).getUsers().add(user.getEmail());
		OrganisationResource.writeOrgs(ctx.getRealPath("/"));
		
		userDB.put(user.getEmail().toLowerCase(), user);			
		writeUsers(ctx.getRealPath("/"));
		return Response.status(200).build();
		
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteUser(@PathParam("id") String id)
	{
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		
		if (checkUser.getRole().equals(Role.COMMON_USER))
			return Response.status(403).build();
		
		if (id == null || id.equals(""))
			return Response.status(403).build();
		
		if (!userDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		
		User deleteUser = userDB.get(id.toLowerCase());
		
		//ne pripada njegovoj organizaciji
		if (checkUser.getRole().equals(Role.ADMIN) && !checkUser.getOrganisationName().equals(deleteUser.getOrganisationName()))
			return Response.status(403).build();
		
		//izbrisati iz liste korisnika organizacije
		String orgId = deleteUser.getOrganisationName().toLowerCase();
		deleteUserFromOrg(id.toLowerCase(), orgId);
		
		//izbrisati korisnika
		userDB.remove(id.toLowerCase());
		writeUsers(ctx.getRealPath("/"));
		return Response.status(200).build();
	
	}
	
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(Credentials credentials) {
		String username = credentials.getUsername();
		String password = credentials.getPassword();
		if (userDB.containsKey(username.toLowerCase())) {
			User u = userDB.get(username.toLowerCase());
			if (u.getPassword().equals(password)) {
				request.getSession().setAttribute("loggedInUser", u);
				return Response.status(200).entity(u).build();
			} else
				return Response.status(400).entity("password;Incorrect password!").build();
		} else
			return Response.status(400).entity("username;User with email " + username + " doesn't exist!").build();

	}

	@POST
	@Path("/logout")
	@Produces(MediaType.TEXT_PLAIN) 
	public Response logout() {
		request.getSession().setAttribute("loggedInUser", null);
		return Response.ok().build();
	}
	
	@PostConstruct
	void init() {
		loadUsers(ctx.getRealPath("/"));
	}
	
	static void loadUsers(String path) {
		if (userDB.isEmpty()) {
			ConcurrentHashMap<String, User> users = UserDb.load(path);
			if (users != null) {
				userDB = users;
				return;
			}
			// ako se desila greska u ucitavanju, ucitaj ove defaultne
			User u = new User("admin@gmail.com", "Adminko", "Adminovic", "", Role.SUPER_ADMIN, "admin");
			User u3 = new User("pera@gmail.com", "Pera", "Peric", "Cloud", Role.ADMIN, "pera");
			User u1 = new User("naca@yahoo.com", "Natasa", "Ivanovic", "Cloud", Role.COMMON_USER, "naca");
			User u2 = new User("maki@gmail.com", "Mario", "Kujundzic", "Rainbow", Role.COMMON_USER, "maki");
			userDB.put(u1.getEmail(), u1);
			userDB.put(u2.getEmail(), u2);
			userDB.put(u3.getEmail(), u3);
			userDB.put(u.getEmail(), u);
			writeUsers(path);
		}
	}
	
	
	public static void writeUsers(String path) {
		UserDb.write(path, userDB);
	}

	public static ConcurrentHashMap<String, User> getDB() {
		return userDB;
	}
	
	private void deleteUserFromOrg(String userId, String orgId)
	{
		Organisation org = OrganisationResource.getDB().get(orgId);
		List<String> users = org.getUsers();
		
		for (String username : users) {
			if (username.toLowerCase().equals(userId)) {
				users.remove(username);
				break;
			}
		}
		org.setUsers(users);
		OrganisationResource.getDB().remove(orgId);
		OrganisationResource.getDB().put(orgId, org);
		OrganisationResource.writeOrgs(ctx.getRealPath("/"));
		
	}


	private String validateEmptyFields(User u) {
		if (u.getEmail() == null || u.getEmail().equals(""))
			  return "email;This is a required field!";
		  if (u.getName() == null || u.getName().equals(""))
			  return "name;This is a required field!";
		  if (u.getSurname() == null || u.getSurname().equals(""))
			  return "surname;This is a required field!";
		  if (u.getRole() == null || u.getRole().toString().equals(""))
			  return "organisation;This is a required field!";
		  if (u.getRole() == null || u.getRole().toString().equals(""))
			  return "role;This is a required field!";
		  if (u.getPassword() == null || u.getPassword().equals(""))
			  return "password;This is a required field!";
		  return "";
	}

}
