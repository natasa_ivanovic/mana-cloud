package resources;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import database.VirtualMachineDb;
import model.Activity;
import model.Disk;
import model.Organisation;
import model.Role;
import model.User;
import model.VMCategory;
import model.VirtualMachine;
import resources.responses.VmResponse;

@Path("/virtualMachines")
public class VirtualMachineResource {
	@Context
	ServletContext ctx;

	@Context
	HttpServletRequest request;

	private static ConcurrentHashMap<String, VirtualMachine> vMachineDB = new ConcurrentHashMap<>();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllVM() {
		// u slucaju da nije ulogovan i da pristupa stranici
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		
		Collection<VmResponse> vMachines = new ArrayList<>();
		for (VirtualMachine vm : vMachineDB.values()) {
			VMCategory category = CategoryResource.getDB().get(vm.getCategory().toLowerCase());
			if (checkUser.getRole() != Role.SUPER_ADMIN
					&& !vm.getOrganisation().equals(checkUser.getOrganisationName()))
				continue;
			VmResponse vmRes = new VmResponse(vm, category);
			vMachines.add(vmRes);
		}
		return Response.status(200).entity(vMachines).build();
	}

	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVM(@PathParam("id") String id) {
		User user = (User) request.getSession().getAttribute("loggedInUser");
		if (user == null)
			return Response.status(403).build();
		
		if (id == null || id.equals(""))
			return Response.status(403).build();
		
		if (!vMachineDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		
		VirtualMachine vm = vMachineDB.get(id.toLowerCase());
		if ((user.getRole().equals(Role.ADMIN) || user.getRole().equals(Role.COMMON_USER))
				&& !user.getOrganisationName().equals(vm.getOrganisation()))
			return Response.status(403).entity("You do not have the access to edit this VM").build();
		/*if ((user.getRole().equals(Role.ADMIN) && !user.getOrganisationName().equals(vm.getOrganisation()))
				|| user.getRole().equals(Role.COMMON_USER))
			return Response.status(403).build();
		*/
		ArrayList<VMCategory> cats = new ArrayList<VMCategory>();
		for (VMCategory cat : CategoryResource.getDB().values()) {
			cats.add(cat);
		}
		VmResponse res = new VmResponse(vm, cats);
		ArrayList<String> disks = new ArrayList<String>();
		for (Disk disk : DiskResource.getDB().values()) {
			// samo dodaj diskove koji su iz iste org i koji vec nisu zakaceni na ovu vm
			if (disk.getOrganisation().equals(vm.getOrganisation())) {
				if (!disk.getVirtualMachine().equals(vm.getName()) && disk.getVirtualMachine().equals(""))
					disks.add(disk.getName());
			}
		}
		res.setDisks(disks);
		return Response.status(200).entity(res).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response addVM(VirtualMachine vm) {
		// check da li je uopste iko ulogovan
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		
		// check da li je super admin
		if (checkUser.getRole().equals(Role.COMMON_USER))
			return Response.status(403).build();
		
		String message = validateFields(vm);
		if (!message.equals(""))
			return Response.status(403).build();
		
		if (vMachineDB.containsKey(vm.getName().toLowerCase()))
			return Response.status(400).entity("name;Name has to be unique!").build();
		vm.setActivities(new ArrayList<Activity>());
		vMachineDB.put(vm.getName().toLowerCase(), vm);
		OrganisationResource.getDB().get(vm.getOrganisation().toLowerCase()).getVirtualMachines().add(vm.getName());
		OrganisationResource.writeOrgs(ctx.getRealPath("/"));
		boolean diskChanged = false;
		for (String disk : vm.getDisks()) {
			DiskResource.getDB().get(disk.toLowerCase()).setVirtualMachine(vm.getName());
			diskChanged = true;
		}
		if (diskChanged)
			DiskResource.writeDisks(ctx.getRealPath("/"));
		writeVms(ctx.getRealPath("/"));
		return Response.ok().build();
	}

	@GET
	@Path("/info")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAddInfo() {
		User user = (User) request.getSession().getAttribute("loggedInUser");
		if (user == null)
			return Response.status(403).build();
		if (user.getRole().equals(Role.COMMON_USER))
			return Response.status(403).build();
		VmResponse res = new VmResponse();
		// dodaj organizacije
		// dodaj kategorije
		// dodaj diskove
		HashMap<String, List<String>> orgs = new HashMap<String, List<String>>();
		ArrayList<VMCategory> cats = new ArrayList<VMCategory>();
		if (user.getRole().equals(Role.SUPER_ADMIN)) {
			for (Organisation org : OrganisationResource.getDB().values()) {
				ArrayList<String> disks = new ArrayList<String>();
				for (Disk disk : DiskResource.getDB().values()) {
					if (disk.getOrganisation().equals(org.getName()) && disk.getVirtualMachine().equals(""))
						disks.add(disk.getName());
				}
				orgs.put(org.getName(), disks);
			}
		} else {
			ArrayList<String> disks = new ArrayList<String>();
			for (Disk disk : DiskResource.getDB().values()) {
				if (disk.getOrganisation().equals(user.getOrganisationName()) && disk.getVirtualMachine().equals(""))
					disks.add(disk.getName());
			}
			orgs.put(user.getOrganisationName(), disks);
		}
		for (VMCategory cat : CategoryResource.getDB().values()) {
			cats.add(cat);
		}
		res.setCategories(cats);
		res.setOrgs(orgs);
		return Response.status(200).entity(res).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response editVM(@PathParam("id") String id, VirtualMachine newVm) {
		User user = (User) request.getSession().getAttribute("loggedInUser");
		if (user == null)
			return Response.status(403).build();
		
		if (!vMachineDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		
		if (user.getRole().equals(Role.COMMON_USER))
			return Response.status(403).build();
		
		VirtualMachine vm = vMachineDB.get(id.toLowerCase());
		if (user.getRole().equals(Role.ADMIN) && !user.getOrganisationName().equals(vm.getOrganisation()))
			return Response.status(403).build();
		
		String message = validateFields(vm);
		if (!message.equals(""))
			return Response.status(403).build();
		
		if (!id.equals(newVm.getName()))
			if (vMachineDB.containsKey(newVm.getName().toLowerCase()))
				return Response.status(400).entity("name;Name has to be unique").build();
		
		// stavi sve end-ove na null zbog lakse provere
		for (Activity a : newVm.getActivities())
			if (a.getTurnedOff().getTime() == 0)
				a.setTurnedOff(null);
		
		for (Activity a : newVm.getActivities()) {
			if (a.getTurnedOff() == null) {
				// posto je ovo otvoren interval, proveravamo svaki drugi da li zavrsava pre
				// nego sto ovaj pocinje
				for (Activity a2 : newVm.getActivities()) {
					if (a.equals(a2))
						continue;
					if (a2.getTurnedOff() == null) {
						// drugi je isto otvoren interval, ne moze biti!
						return Response.status(400).entity("#error;There cannot be two activities that aren't finished!")
								.build();
					} else {
						// drugi je zatvoren interval, mora biti zavrsen pre pocetka prvog
						if (a2.getTurnedOff().after(a.getTurnedOn()))
							return Response.status(400).entity("#error;Activity can't end after a new activity has begun!")
									.build();
						if (a.getTurnedOn().getTime() - a2.getTurnedOff().getTime() < 3600000)
							return Response.status(400).entity("#error;Activity can't end on the same hour!").build();
					}
				}
			} else {
				// prvi je zatvoren interval
				if (a.getTurnedOff().before(a.getTurnedOn()) || a.getTurnedOff().getTime() - a.getTurnedOn().getTime() < 3600000)
					return Response.status(400).entity("#error;Activity can't end before it has begun!").build();

				for (Activity a2 : newVm.getActivities()) {
					if (a.equals(a2))
						continue;
					// drugi je otvoren interval, mora da je poceo nakon sto se ovaj zavrsio
					if (a2.getTurnedOff() == null) {
						if (a2.getTurnedOn().before(a.getTurnedOff()))
							return Response.status(400)
									.entity("#error;Activity can't be unfinished if it started during another activity")
									.build();
						if (a2.getTurnedOn().getTime() - a.getTurnedOff().getTime() < 3600000)
							return Response.status(400).entity("#error;Activity can't begin on the same hour!").build();
					} else {
						// drugi je zatvoren interval, complex provera
						if (a.getTurnedOn().before(a2.getTurnedOff()) && a.getTurnedOff().after(a2.getTurnedOn()))
							return Response.status(400).entity("#error;Two finished activites cannot overlap!").build();
						if (a.getTurnedOn().getTime() - a2.getTurnedOff().getTime() < 3600000 &&
								a2.getTurnedOn().getTime() - a.getTurnedOff().getTime() < 3600000)
							return Response.status(400).entity("#error;Two finished activites cannot overlap within the hour!").build();
					}
				}
			}
		}
		if (!id.equals(newVm.getName())) {
			// menja se ime, brisi iz recnika i opet stavi
			vm.setCategory(newVm.getCategory());
			vm.setName(newVm.getName());
			vMachineDB.remove(id.toLowerCase());
			vMachineDB.put(vm.getName().toLowerCase(), vm);
			// pisanje org opet ako su imale ref, diskove cemo posle jer svakako treba
			// proveriti koji su se menjali
			OrganisationResource.getDB().get(vm.getOrganisation().toLowerCase()).getVirtualMachines().remove(id);
			OrganisationResource.getDB().get(vm.getOrganisation().toLowerCase()).getVirtualMachines()
					.add(newVm.getName());
			OrganisationResource.writeOrgs(ctx.getRealPath("/"));
		} else {
			// ne menja se ime, samo zameni stvari
			vm.setCategory(newVm.getCategory());
		}
		// proslo je sve sto moze po zlu da podje, setuj aktivnosti na nove
		vm.setActivities(newVm.getActivities());
		// provera diskova, koji su se menjali, pretpostavka da nisu
		boolean disksChanged = false;
		// prodji kroz stare, proveri da li postoje u novom
		for (String disk : vm.getDisks()) {
			if (newVm.getDisks().contains(disk)) {
				// sadrzi disk, samo promeni ref u disku ako se menjalo ime
				if (!id.equals(newVm.getName())) {
					DiskResource.getDB().get(disk.toLowerCase()).setVirtualMachine(newVm.getName());
					disksChanged = true;
				}
			} else {
				// ne sadrzi disk, odvezi disk od VM, kasnije ces dodeliti novu listu trenutnoj
				// vm svakako
				DiskResource.getDB().get(disk.toLowerCase()).setVirtualMachine("");
				disksChanged = true;
			}
		}
		// svim novim diskovima (koji pre nisu bili) stavi novu vm za vm polje
		for (String disk : newVm.getDisks()) {
			if (!vm.getDisks().contains(disk)) {
				// samo gledamo one koji su novi dodati
				DiskResource.getDB().get(disk.toLowerCase()).setVirtualMachine(newVm.getName());
				disksChanged = true;
			}
		}
		vm.setDisks(newVm.getDisks());
		// pisi u fajl
		if (disksChanged)
			DiskResource.writeDisks(ctx.getRealPath("/"));
		writeVms(ctx.getRealPath("/"));
		return Response.status(200).build();
	}

	@Path("/{id}")
	@DELETE
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteVM(@PathParam("id") String id) {
		User user = (User) request.getSession().getAttribute("loggedInUser");
		if (user == null)
			return Response.status(403).build();
		
		if (id == null || id.equals(""))
			return Response.status(403).build();
		
		if (!vMachineDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		
		VirtualMachine vm = vMachineDB.get(id.toLowerCase());
		if (user.getRole().equals(Role.COMMON_USER)
				|| (user.getRole().equals(Role.ADMIN) && !user.getOrganisationName().equals(vm.getOrganisation())))
			return Response.status(403).build();
		
		boolean disksChanged = false;
		for (String disk : vm.getDisks()) {
			if (DiskResource.getDB().containsKey(disk.toLowerCase())) {
				DiskResource.getDB().get(disk.toLowerCase()).setVirtualMachine("");
				disksChanged = true;
			}
		}
		// skloni iz mape
		vMachineDB.remove(id.toLowerCase());
		// skloni iz org i pisi org na disk
		OrganisationResource.getDB().get(vm.getOrganisation().toLowerCase()).getVirtualMachines().remove(id);
		OrganisationResource.writeOrgs(ctx.getRealPath("/"));
		// pisi na disk
		writeVms(ctx.getRealPath("/"));
		if (disksChanged)
			DiskResource.writeDisks(ctx.getRealPath("/"));
		return Response.ok().build();
	}
	
	
	@PUT
	@Path("/activity/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response activateVirtualMachine(@PathParam("id") String id) {
		
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		
		if(!checkUser.getRole().equals(Role.ADMIN))
			return Response.status(403).build();
		
		if(id == null || id.equals(""))
			return Response.status(403).build();
		
		if (!vMachineDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		
		VirtualMachine vm = vMachineDB.get(id.toLowerCase());
		
		
		for (Activity a : vm.getActivities()) {
			if (a.getTurnedOff() == null)
			{
				Date newDate = new Date();
				Date oldDate = a.getTurnedOn();
				int period = checkHour(oldDate, newDate);
				
				if(period > 1) {
					a.setTurnedOff(newDate);
					writeVms(ctx.getRealPath("/"));
					return Response.status(200).entity(vm).build();
				} else
					return Response.status(400).entity("#error;This virtual machine has already been turned on in this hour").build();	
			}
		}
			
		Activity newAct = new Activity();
		Date newDate = new Date();
		Date oldDate;
		//getuj poslednju aktivnost
		if (!vm.getActivities().isEmpty()) {
			oldDate = vm.getActivities().get(vm.getActivities().size()-1).getTurnedOff();
			int period = checkHour(oldDate, newDate);
			
			if (period > 1) {
				newAct.setTurnedOn(newDate);
				vm.getActivities().add(newAct);
				writeVms(ctx.getRealPath("/"));
				return Response.status(200).entity(vm).build();
			}
			else
				return Response.status(400).entity("#error;This virtual machine has already been turned off/on in this hour").build();
		}
		else {
			//ne postoji jos nijedna aktivnost i slobodno mozemo dodati
			newAct.setTurnedOn(newDate);
			vm.getActivities().add(newAct);
			writeVms(ctx.getRealPath("/"));
			return Response.status(200).entity(vm).build();
		}
			
		
		
	}

	@PostConstruct
	public void init() {
		loadVms(ctx.getRealPath("/"));
		OrganisationResource.loadOrgs(ctx.getRealPath("/"));
		CategoryResource.loadCats(ctx.getRealPath("/"));
		DiskResource.loadDisks(ctx.getRealPath("/"));
		UserResource.loadUsers(ctx.getRealPath("/"));
	}

	static void loadVms(String path) {
        if (vMachineDB.isEmpty()) {
            ConcurrentHashMap<String, VirtualMachine> machines = VirtualMachineDb.load(path);
            if (machines != null) {
                vMachineDB = machines;
                return;
            }
            // ako se desila greska u ucitavanju, ucitaj ove defaultne
            VirtualMachine vm = new VirtualMachine("VM1", "Gaming", "Cloud");
 
            ArrayList<Activity> activities = new ArrayList<Activity>();
            Activity a = new Activity();
            a.setTurnedOn(new GregorianCalendar(2018, Calendar.FEBRUARY, 11).getTime());
            a.setTurnedOff(new GregorianCalendar(2019, Calendar.MARCH, 23).getTime());
            activities.add(a);
           
            Activity a2 = new Activity();
            a2.setTurnedOn(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
            activities.add(a2);
            vm.setActivities(activities);
           
            String d = "SG300BQ";
            vm.getDisks().add(d);
            vMachineDB.put(vm.getName().toLowerCase(), vm);
           
            VirtualMachine vm2 = new VirtualMachine("VM2", "Enterprise", "Rainbow");
 
            Activity a3 = new Activity();
            a3.setTurnedOn(new GregorianCalendar(2019, Calendar.SEPTEMBER, 23).getTime());
            a3.setTurnedOff(new GregorianCalendar(2019, Calendar.DECEMBER, 22).getTime());
            vm2.getActivities().add(a3);
 
            String d2 = "Samsung EVO820";
            vm2.getDisks().add(d2);
            vMachineDB.put(vm2.getName().toLowerCase(), vm2);
           
            writeVms(path);
        }
    }
	
	public static void writeVms(String path) {
		VirtualMachineDb.write(path, vMachineDB);
	}

	public static Map<String, VirtualMachine> getDB() {
		return vMachineDB;
	}
	
	private int checkHour(Date oldDate, Date newDate) {
		//1000->sekunde; 60->minute; 60->sati
		 int hours = 1000 * 60 * 60;
		 int period = (int) ((newDate.getTime() - oldDate.getTime()) / hours);
		 return period;
		 
	}
	
	private String validateFields(VirtualMachine vm) {
		if (vm.getName() == null || vm.getName().equals(""))
			return "name;This is a required field!";
		if (vm.getCategory() == null || vm.getCategory().equals(""))
			return "category;This is a required field!";
		return "";
	}
}
