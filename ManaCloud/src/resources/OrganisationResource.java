package resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.tomcat.util.codec.binary.Base64;

import database.OrganisationDb;
import model.Activity;
import model.Disk;
import model.DiskType;
import model.Organisation;
import model.Role;
import model.User;
import model.VMCategory;
import model.VirtualMachine;
import resources.requests.InvoiceParams;
import resources.responses.InvoiceResponse;
import resources.responses.OrgResponse;

@Path("/organisations")
public class OrganisationResource {
	
	@Context
	ServletContext ctx;
	
	@Context
	HttpServletRequest request;
	
	private static ConcurrentHashMap<String, Organisation> orgDB = new ConcurrentHashMap<String, Organisation>();
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrganisation()
	{
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build(); //nije ulogovan
		
		List<OrgResponse> orgs = new ArrayList<>();
		if (checkUser.getRole().equals(Role.SUPER_ADMIN))
		{
			for (Organisation o : orgDB.values()) {
				String orgSend = "default.jpg";
				if (!o.getLogo().equals("default.jpg")) 
					orgSend = getImageEncoded(o.getLogo());
				OrgResponse response = new OrgResponse(o.getName(), o.getDescription(), orgSend);
				orgs.add(response);
			}
			return Response.status(200).entity(orgs).build();
		}
		/*else if (checkUser.getRole().equals(Role.ADMIN)) 
		{
			String orgName = checkUser.getOrganisationName().toLowerCase();
			if (!orgDB.containsKey(orgName))
				return Response.status(403).build();
			Organisation org = orgDB.get(orgName);
			String orgSend = "default.jpg";
			if (!org.getLogo().equals("default.jpg")) 
				orgSend = getImageEncoded(org.getLogo());
			OrgResponse response = new OrgResponse(org.getName(), org.getDescription(), orgSend);
			orgs.add(response);
			return Response.status(200).entity(orgs).build();
			
		}*/
		//nema pravo pristupa
		return Response.status(403).build();
	}

	
	@PUT
	@Path("/{name}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response editOrganisation(@PathParam("name") String name, Organisation recievedOrg)
	{
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build(); //nije ulogovan
		
		if (name == null || name.equals(""))
			return Response.status(403).build();
		
		if (checkUser.getRole().equals(Role.COMMON_USER))
			return Response.status(403).build();
		
		//admin menja organizaciju kojoj ne pripada
		if (checkUser.getRole().equals(Role.ADMIN) && !checkUser.getOrganisationName().equals(name))
			return Response.status(403).build();
		
		if (!orgDB.containsKey(name.toLowerCase()))
			return Response.status(403).build(); // ne postoji ta organizacija
		
		if (name == null || "".equals(name))
			return Response.status(400).entity("name;This is a required field!").build();
		
		String newName = recievedOrg.getName().toLowerCase();
		//ako to ime postoji u mapi, a nije staro ime
		if (orgDB.containsKey(newName) && !newName.equals(name.toLowerCase()))
			return Response.status(400).entity("name;Name has to be unique!").build();
		
		Organisation organisation = orgDB.get(name.toLowerCase());
		
		organisation.setName(recievedOrg.getName());
		organisation.setDescription(recievedOrg.getDescription());
		
		String logo = recievedOrg.getLogo();
		String oldLogo = organisation.getLogo();
		
		File f = new File(ctx.getRealPath("/") + oldLogo);
		if (f.exists())
			f.delete();

		if (logo == null || ("").equals(logo) || "default.jpg".equals(logo))
			organisation.setLogo("default.jpg");
		else
		{
			String ext = logo.substring(logo.indexOf("/") +1, logo.indexOf(";"));
			String path = "/../ManaCloudDatabase/" + organisation.getName() + "." + ext; 
			organisation.setLogo(path);
			saveLogoToFile(logo, path);
		}
		
		//updejt mape sa organizacijama
		orgDB.remove(name.toLowerCase());
		orgDB.put(organisation.getName().toLowerCase(), organisation);
		
		//pisanje izmena u fajl
		writeOrgs(ctx.getRealPath("/"));
		
		//update korisnika, virtualnih masina i diskova
		if (!name.toLowerCase().equals(recievedOrg.getName().toLowerCase()))
		{
			updateUsers(name, recievedOrg.getName());
			updateVM(name, recievedOrg.getName());
			updateDisk(name, recievedOrg.getName());
			return Response.status(200).entity(recievedOrg.getName()).build();
		}
		
		return Response.status(200).build();
		
	}
	
	@GET
	@Path("/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrganisationEdit(@PathParam("name") String name)
	{
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build(); //nije ulogovan
		
		if (name == null || name.equals(""))
			return Response.status(403).build();
		
		
		if (!orgDB.containsKey(name.toLowerCase()))
			return Response.status(403).build();
			
		Organisation org = orgDB.get(name.toLowerCase());
		
		if (!checkUser.getOrganisationName().equals(org.getName()) && checkUser.getRole().equals(Role.ADMIN))
			return Response.status(403).build();
		
		String orgSend = "default.jpg";
		
		if (!org.getLogo().equals("default.jpg")) 
			orgSend = getImageEncoded(org.getLogo());			
		else
			orgSend = "default.jpg";
		OrgResponse response = new OrgResponse(org.getName(), org.getDescription(), orgSend);
		return Response.status(200).entity(response).build();
	}
	
	private String getImageEncoded(String path) {
		String filePath = ctx.getRealPath("/") + path;
		File f = new File(filePath);		
		try (FileInputStream fis = new FileInputStream(f)) {
			byte byteArray[] = new byte[(int)f.length()];
			fis.read(byteArray);
			String imageString = Base64.encodeBase64String(byteArray);
			// dodaj image/{ext};base64,
			String ext = path.substring(path.lastIndexOf(".") + 1);
			imageString = "data:image/" + ext + ";base64," + imageString;
			return imageString;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "default.jpg";
	}

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response addOrganisation(Organisation org)
	{
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build(); //nije ulogovan
		
		if (!checkUser.getRole().equals(Role.SUPER_ADMIN))
			return Response.status(403).entity("Forbidden!").build(); //nedozvoljena akcija
		
		if (org.getName().equals(""))
			return Response.status(403).entity("This is a required field!").build();
		
		if (orgDB.containsKey(org.getName().toLowerCase()))
				return Response.status(400).entity("Name has to be unique!").build();
		
		String logo = org.getLogo();
		
		if (logo.equals(""))
			org.setLogo("default.jpg");
			//org.setLogo("/../ManaCloudDatabase/pictures/default.jpg");
		else {
			String ext = logo.substring(logo.indexOf("/") +1, logo.indexOf(";"));
			String path = "/../ManaCloudDatabase/" + org.getName() + "." + ext; 
			org.setLogo(path);
			saveLogoToFile(logo, path);
		}
		
		org.setDisks(new ArrayList<String>());
		org.setUsers(new ArrayList<String>());
		org.setVirtualMachines(new ArrayList<String>());
		System.out.println(org.getLogo());
		orgDB.put(org.getName().toLowerCase(), org);
		
		writeOrgs(ctx.getRealPath("/"));
		return Response.status(200).build();
		
		
	}
	


	@Path("/{name}")
	@DELETE
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteOrg(@PathParam("name") String name)
	{
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).entity("You aren't authorized to be here!").build();
		
		if (!checkUser.getRole().equals(Role.SUPER_ADMIN))
			return Response.status(403).entity("You aren't authorized to delete this organisation!").build();
	
		if (!orgDB.containsKey(name.toLowerCase()))
			return Response.status(403).entity("Organisation with ID " + name + " doesn't exist!").build();
		
		//provera korisnika, virtualnih masina, diskova
		if (checkUsers(name) == true || checkVM(name) == true || checkDisk(name) == true)
			return Response.status(403).entity("Unable to delete").build();
		
		orgDB.remove(name.toLowerCase());
		writeOrgs(ctx.getRealPath("/"));
		return Response.status(200).entity("Successfully deleted").build();
	
	}
	
	@Path("/invoice")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response invoice(InvoiceParams params) {
		// proveri da li je ok zahtev
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).entity("You aren't authorized to be here!").build();
		
		if (!checkUser.getRole().equals(Role.ADMIN))
			return Response.status(403).build();
	
		if (params.getFromDate() == null || params.getToDate() == null)
			return Response.status(400).entity("#error;Starting and ending dates can't be empty!").build();
		
		if (params.getFromDate().after(params.getToDate()))
			return Response.status(400).entity("#error;Starting date must be before ending date!").build();
		
		// VM cene - za kada je bila upaljena
		// 25 * broj jezgara mesecno = 25 / (30 dana * 24h) =  0.03472222
		// 15 * giga rama = 0.02083333
		// 1 * gpu core = 0.00138889
		
		Organisation org = orgDB.get(checkUser.getOrganisationName().toLowerCase());
		ArrayList<InvoiceResponse> response = new ArrayList<InvoiceResponse>();
		
		for (String v : org.getVirtualMachines()) {
			double hours = 0;
			VirtualMachine vm = VirtualMachineResource.getDB().get(v.toLowerCase());
			for (Activity act : vm.getActivities()) {
				if (act.getTurnedOff() == null)
					continue;
				// intersect formula - zelimo da posmatramo samo aktivnosti koje imaju preklapanje sa range-om
				if (params.getFromDate().before(act.getTurnedOff()) && params.getToDate().after(act.getTurnedOn())) {
					if (params.getFromDate().before(act.getTurnedOn()) && params.getToDate().before(act.getTurnedOff())) {
						// pocetak posmatranog intervala je pre pocetka rada, a kraj intervala je pre kraja rada
						hours += (params.getToDate().getTime() - act.getTurnedOn().getTime()) / (1000.0*60.0*60.0);
					} else if (params.getFromDate().after(act.getTurnedOn()) && params.getToDate().after(act.getTurnedOff())) {
						// pocetak posm. intervala je posle pocetka rada, a kraj nakon kraja rada
						hours += (act.getTurnedOff().getTime() - params.getFromDate().getTime()) / (1000.0*60.0*60.0);
					} else if (params.getFromDate().after(act.getTurnedOn()) && params.getToDate().before(act.getTurnedOff())) {
						// pocetak intervala je nakon pocetka, kraj pre kraja
						hours += (params.getToDate().getTime() - params.getFromDate().getTime()) / (1000.0*60.0*60.0);
					} else {
						// pocetak posm. int je pre pocetka, kraj int je nakon kraja
						// oduzmemo end i start time, dobijemo milisekunde, pa delimo sa 1000 * 60 * 60 (ms u s, s u min, min u hr)
						hours += (act.getTurnedOff().getTime() - act.getTurnedOn().getTime()) / (1000.0*60.0*60.0);					
					}									
				}
			}
			VMCategory cat = CategoryResource.getDB().get(vm.getCategory().toLowerCase());
			double pricePerHour = 0.03472222 * cat.getCores() + 0.02083333 * cat.getRam() + 0.00138889 * cat.getGpu();
			double totalPrice = pricePerHour * hours;
			InvoiceResponse ir = new InvoiceResponse(v, "VM", hours, pricePerHour, totalPrice);
			response.add(ir);
		}
		// disk cene - stalno
		// hdd - 0.1 * gb = 0.00013888
		// ssd - 0.3 * gb = 0.00041666
		for (String v : org.getDisks()) {
			double hours = (params.getToDate().getTime() - params.getFromDate().getTime()) / (1000.0*60.0*60.0);
			Disk d = DiskResource.getDB().get(v.toLowerCase());
			double pricePerHour = 0;
			if (d.getType().equals(DiskType.SSD))
				pricePerHour = 0.00041666 * d.getCapacity();
			else
				pricePerHour = 0.00013888 * d.getCapacity();
			double totalPrice = pricePerHour * hours;
			InvoiceResponse ir = new InvoiceResponse(v, "Disk", hours, pricePerHour, totalPrice);
			response.add(ir);
		}
		return Response.status(200).entity(response).build();
	}
	
	
	
	
	private boolean checkUsers(String id)
	{
		for (User u : UserResource.getDB().values()) {
			if (u.getOrganisationName().equals(id))
				return true;
		}
		return false;
	}
	
	private boolean checkVM(String id)
	{
		for (VirtualMachine vm : VirtualMachineResource.getDB().values()) {
			if (vm.getOrganisation().equals(id))
				return true;
		}
		return false;
	}
	
	private boolean checkDisk(String id)
	{
		for (Disk d : DiskResource.getDB().values()) {
			if (d.getOrganisation().equals(id))
				return true;
		}
		return false;
	}
	
	private void updateUsers(String name, String newName)
	{
		for (User u : UserResource.getDB().values()) {
			if (u.getOrganisationName().equals(name))
				u.setOrganisationName(newName);
		}
		UserResource.writeUsers(ctx.getRealPath("/"));
	}
	
	private void updateVM(String name, String newName)
	{
		for (VirtualMachine vm : VirtualMachineResource.getDB().values()) {
			if (vm.getOrganisation().equals(name))
				vm.setOrganisation(newName);
		}
		VirtualMachineResource.writeVms(ctx.getRealPath("/"));
	}
	
	private void updateDisk(String name, String newName)
	{
		for (Disk d : DiskResource.getDB().values()) {
			if (d.getOrganisation().equals(name))
				d.setOrganisation(newName);
		}
		DiskResource.writeDisks(ctx.getRealPath("/"));
		
	}
	
	private void saveLogoToFile(String logo, String path) {
		//'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPAAAADwCAYAAAA+VemSAAAgAEl...=='
		byte[] data = Base64.decodeBase64(logo.split(",")[1]);
		String filePath = ctx.getRealPath("/") + path;
		try (OutputStream stream = new FileOutputStream(filePath)) {
			stream.write(data);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@PostConstruct
	private void init() {
		//loadOrgs(this.getClass().getResource("/").getPath());
		loadOrgs(ctx.getRealPath(""));
	}
	
	static void loadOrgs(String path) {
		if (orgDB.isEmpty()) {
			ConcurrentHashMap<String, Organisation> orgs = OrganisationDb.load(path);
			if (orgs != null) {
				orgDB = orgs;
				return;
			}
			// ako se desila greska u ucitavanju, ucitaj ove defaultne
			Organisation o = new Organisation("Cloud", "Everything you need", "default.jpg");
			Organisation o1 = new Organisation("Rainbow", "Neki opis", "default.jpg");
			o.getVirtualMachines().add("VM1");
			o1.getVirtualMachines().add("VM2");
			o.getDisks().add("SG300BQ");
			o1.getDisks().add("Samsung EVO820");
			orgDB.put(o.getName().toLowerCase(), o);
			orgDB.put(o1.getName().toLowerCase(), o1);
			
			for (User u : UserResource.getDB().values())
			{
				String organisation = u.getOrganisationName();
				if (orgDB.containsKey(organisation.toLowerCase()))
				{
					orgDB.get(organisation.toLowerCase()).getUsers().add(u.getEmail());
				}
					
			}
			writeOrgs(path);
		}
	}

	public static void writeOrgs(String path) {
		OrganisationDb.write(path, orgDB);
	}
	
	public static Map<String, Organisation> getDB() {
		return orgDB;
	}
}
