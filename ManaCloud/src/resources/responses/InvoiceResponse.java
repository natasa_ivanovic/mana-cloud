package resources.responses;

public class InvoiceResponse {
	private String id;
	private String type;
	private double hours;
	private double pricePerHour;
	private double totalPrice;
	
	public InvoiceResponse() {}

	public InvoiceResponse(String id, String type, double hours, double pricePerHour, double totalPrice) {
		super();
		this.id = id;
		this.type = type;
		this.hours = hours;
		this.pricePerHour = pricePerHour;
		this.totalPrice = totalPrice;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getHours() {
		return hours;
	}

	public void setHours(double hours) {
		this.hours = hours;
	}

	public double getPricePerHour() {
		return pricePerHour;
	}

	public void setPricePerHour(double pricePerHour) {
		this.pricePerHour = pricePerHour;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	
}

