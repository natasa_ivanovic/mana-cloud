package resources.responses;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import model.VMCategory;
import model.VirtualMachine;

public class VmResponse {
	private VirtualMachine vMachine;
	private VMCategory category;
	private List<VMCategory> categories;
	private List<String> disks;
	private HashMap<String, List<String>> orgs;
	public VmResponse() {}

	public VmResponse(VirtualMachine vMachine, VMCategory category) {
		super();
		this.vMachine = vMachine;
		this.category = category;
		
	}
	
	public VmResponse(VirtualMachine vMachine, List<VMCategory> cats) {
		super();
		this.vMachine = vMachine;
		this.categories = cats;
		this.disks = new ArrayList<String>();
	}

	public VirtualMachine getvMachine() {
		return vMachine;
	}

	public void setvMachine(VirtualMachine vMachine) {
		this.vMachine = vMachine;
	}

	public VMCategory getCategory() {
		return category;
	}

	public void setCategory(VMCategory category) {
		this.category = category;
	}

	public List<VMCategory> getCategories() {
		return categories;
	}

	public void setCategories(List<VMCategory> categories) {
		this.categories = categories;
	}

	public List<String> getDisks() {
		return disks;
	}

	public void setDisks(List<String> disks) {
		this.disks = disks;
	}

	public HashMap<String, List<String>> getOrgs() {
		return orgs;
	}

	public void setOrgs(HashMap<String, List<String>> orgs) {
		this.orgs = orgs;
	}

	
	
	
	
}
