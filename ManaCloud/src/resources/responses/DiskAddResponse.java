package resources.responses;

import java.util.ArrayList;
import java.util.HashMap;

import model.Disk;

public class DiskAddResponse {
	HashMap<String, ArrayList<String>> orgs;
	ArrayList<String> vms;
	Disk disk;
	
	public DiskAddResponse() {
		orgs = new HashMap<String, ArrayList<String>>();
		vms = new ArrayList<String>();
	}

	public DiskAddResponse(Disk d) {
		orgs = new HashMap<String, ArrayList<String>>();
		vms = new ArrayList<String>();
		disk = d;
	}
	


	public HashMap<String, ArrayList<String>> getOrgs() {
		return orgs;
	}

	public void setOrgs(HashMap<String, ArrayList<String>> orgs) {
		this.orgs = orgs;
	}

	public ArrayList<String> getVms() {
		return vms;
	}

	public void setVms(ArrayList<String> vms) {
		this.vms = vms;
	}

	public Disk getDisk() {
		return disk;
	}

	public void setDisk(Disk disk) {
		this.disk = disk;
	}

	
}
