package resources.responses;


public class GenericResponse {
	private String user;
	private String response;

	public GenericResponse() {}

	public GenericResponse(String user, String response) {
		super();
		this.user = user;
		this.response = response;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
}