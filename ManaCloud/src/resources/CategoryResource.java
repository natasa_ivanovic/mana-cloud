package resources;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import database.CategoryDb;
import model.Role;
import model.User;
import model.VMCategory;
import model.VirtualMachine;
import resources.responses.GenericResponse;

@Path("/categories")
public class CategoryResource {
	@Context
	ServletContext ctx;

	@Context
	HttpServletRequest request;

	private static ConcurrentHashMap<String, VMCategory> catDB = new ConcurrentHashMap<>();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response addCategory(VMCategory cat) {
		// check da li je uopste iko ulogovan
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		// check da li je super admin
		if (!checkUser.getRole().equals(Role.SUPER_ADMIN))
			return Response.status(403).build();
		
		String message = validateFields(cat);
		if (!message.equals(""))
			return Response.status(400).entity(message).build();
		
		if (catDB.containsKey(cat.getName().toLowerCase()))
			return Response.status(400).entity("name;Entered category name is already in use!").build();
		// dodaj u bazu
		catDB.put(cat.getName().toLowerCase(), cat);

		writeCats(ctx.getRealPath("/"));
		return Response.ok().build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCategories() {
		// u slucaju da nije ulogovan i da pristupa stranici
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		if (!checkUser.getRole().equals(Role.SUPER_ADMIN))
			return Response.status(403).build();
		return Response.status(200).entity(catDB.values()).build();
	}

	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCategory(@PathParam("id") String id) {
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		
		if (!checkUser.getRole().equals(Role.SUPER_ADMIN))
			return Response.status(403).build();
		
		if (id.equals("") || id == null)
			return Response.status(403).build();
		
		if (!catDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		
		VMCategory cat = catDB.get(id.toLowerCase());
		return Response.status(200).entity(cat).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response editCategory(@PathParam("id") String id, VMCategory newCat) {
		User checkUser = (User) request.getSession().getAttribute("loggedInUser");
		if (checkUser == null)
			return Response.status(403).build();
		// check da li je super admin
		if (!checkUser.getRole().equals(Role.SUPER_ADMIN))
			return Response.status(403).build();
		
		if (id == null || id.equals(""))
			return Response.status(403).build();
		
		if (!catDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		
		String message = validateFields(newCat);
		if (!message.equals(""))
			return Response.status(403).build();
		
		VMCategory cat = catDB.get(id.toLowerCase());
		if (!id.equals(newCat.getName())) {
			// menja se ime, prvo proveri jel vec ima neki sa novim imenom
			if (catDB.containsKey(newCat.getName().toLowerCase()))
				return Response.status(400).entity("name;Name has to be unique!").build();
			
			//brisi iz recnika i opet stavi
			cat.setCores(newCat.getCores());
			cat.setGpu(newCat.getGpu());
			cat.setName(newCat.getName());
			cat.setRam(newCat.getRam());
			catDB.remove(id.toLowerCase());
			catDB.put(cat.getName().toLowerCase(), cat);
			// promeni na VM koje su imale stari id na novi, i pisi u fajl
			for (VirtualMachine vm : VirtualMachineResource.getDB().values()) {
				if (vm.getCategory().equals(id))
					vm.setCategory(newCat.getName());
			}
			VirtualMachineResource.writeVms(ctx.getRealPath("/"));
		} else {
			// ne menja se ime, samo zameni stvari
			cat.setCores(newCat.getCores());
			cat.setGpu(newCat.getGpu());
			cat.setRam(newCat.getRam());
		}
		// pisi u fajl
		writeCats(ctx.getRealPath("/"));
		return Response.status(200).build();
	}
	
	@Path("/{id}")
	@DELETE
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteCat(@PathParam("id") String id) {
		User user = (User) request.getSession().getAttribute("loggedInUser");
		if (user == null)
			return Response.status(403).build();
		
		if (!user.getRole().equals(Role.SUPER_ADMIN))
			return Response.status(403).build();
		
		if (id == null || id.equals(""))
			return Response.status(403).build();
		
		if (!catDB.containsKey(id.toLowerCase()))
			return Response.status(403).build();
		// proveri da li se koristi na nekim vm
		for (VirtualMachine vm : VirtualMachineResource.getDB().values()) {
			if (vm.getCategory().equals(id))
				return Response.status(400).entity("name;Category is being used! Unable to delete!").build();
		}
		// skloni iz mape
		catDB.remove(id.toLowerCase());
		writeCats(ctx.getRealPath("/"));
		return Response.ok().build();
	}


	@PostConstruct
	public void init() {
		loadCats(ctx.getRealPath("/"));
	}
	
	static void loadCats(String path) {
		if (catDB.isEmpty()) {
			ConcurrentHashMap<String, VMCategory> cats = CategoryDb.load(path);
			if (cats != null) {
				catDB = cats;
				return;
			}
			// ako se desila greska u ucitavanju, ucitaj ove defaultne
			VMCategory c = new VMCategory("Gaming", 8, 16, 16);
			catDB.put(c.getName().toLowerCase(), c);
			VMCategory c2 = new VMCategory("Enterprise", 16, 32, 0);
			catDB.put(c2.getName().toLowerCase(), c2);
			writeCats(path);
		}
	}

	public static void writeCats(String path) {
		CategoryDb.write(path, catDB);
	}
	

	public static Map<String, VMCategory> getDB() {
		return catDB;
	}
	
	private String validateFields(VMCategory cat) {
		if (cat.getName() == null || cat.getName().equals(""))
			return "name;This is a required field!";
		if (cat.getCores() <= 0)
			return "cores;Value greater than 0 requred!";
		if (cat.getRam() <= 0)
			return "ram;Value greater than 0 required!";
		if (cat.getGpu() < 0)
			return "gpu;Value greater or equal to 0 required!";
		return "";
	}
}
